// Creates an item on the PC that is tied to the current dungeon.
// /!\ Does not work with stackable items
//
// sResref: Item resref
// sAction: Action to trigger when the item leaves the area. If empty, the action will be "destroy"
#include "cleaner_inc"
void main(string sResref, string sAction)
{
	object oPC = GetPCSpeaker();
	if(sAction == "")
		sAction = "destroy";

	object oItem = CreateItemOnObject(sResref, oPC);
	if(!GetIsObjectValid(oItem))
		_Cleaner_SignalBug("cleaner_dial_ga_giveitem: Cannot create item resref '"+sResref+"' on "+_Cleaner_ObjectInfo(oPC)+" (in dialog with "+_Cleaner_ObjectInfo(OBJECT_SELF)+")");
	Cleaner_RegisterItemOnCleanAction(oItem, GetArea(oPC), sAction);
}