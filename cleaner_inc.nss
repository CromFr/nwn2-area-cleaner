#include "x0_i0_position"

// ==================================================
// CLEANER CONFIG
// ==================================================

#include "cleaner_config"


// ==================================================
// CLEANER LIBRARY
// ==================================================


// Triggered once per reboot, before the area state is saved
const int CLEANER_EVENT_MODULE_INIT = 13370;

// Triggered when the first PC enters the dungeon, after the area is init by the Cleaner
const int CLEANER_EVENT_INIT = 13371;

// Triggered when the last PC exits the dungeon, before the area is cleaned by the Cleaner
const int CLEANER_EVENT_CLEAN = 13372;

// Triggered when any PC enters the dungeon
const int CLEANER_EVENT_ENTER = 13373;

// Triggered when any PC exits the dungeon
const int CLEANER_EVENT_EXIT = 13374;


const string CLEANER_ACTION_DESTROY = "destroy";
const string CLEANER_ACTION_SCRIPT = "script";
const string CLEANER_ACTION_EVENT = "event";
const string CLEANER_ACTION_DELETEVAR = "deletevar";
const string CLEANER_ACTION_SEF = "sef";
const string CLEANER_ACTION_USEABLE = "useable";
const string CLEANER_ACTION_LIGHT = "light";
const string CLEANER_ACTION_SOUND = "sound";
const string CLEANER_ACTION_FLOATINGTEXT = "floatingtext";
const string CLEANER_ACTION_ENCOUNTER = "encounter";



// Private functions decl

int _Cleaner_Watchdog_GetHasCleanLocks(object oMasterArea);
void _Cleaner_CleanSingleArea(object oArea);
string _Cleaner_ObjectInfo(object o);
void _Cleaner_DestroyObjectAndInventory(object oObject, float fDelay=0.0);
void _Cleaner_BroadcastMsg(string sMessage);
void _Cleaner_CheckItem(object oItem);


//=============================================================================
// Multi-area (dungeon) system
//=============================================================================
//
// Init/clean all linked areas if a PC enters/exit any of the areas of the dungeon
//
// A dungeon is defined by a master area (that contains all cleaner variables)
// and multiple slave areas.
//
// You can manually add slave maps using Cleaner_AddSlaveArea, but it is
//     usually better to set local variables so multi-area is setup
//     automatically when any area is entered
//
// Local var on the master area:
//   - cleaner_multiarea_slavestags (string): array containing the tags of the
//     linked slave areas, separated with | characters
//         example: |area_tag1|area_tag2|
//
// Local var on the slave areas:
//   - cleaner_multiarea_mastertag (string): Tag of the master area
//
// You can define sub areas (trigger) that belongs to another area's dungeon,
// the same way you add a an area (ie add trigger tag to
// cleaner_multiarea_slavestags and set local var cleaner_multiarea_mastertag
// on the trigger). For the trigger to initialize the dungeon when entered,
// you need to set the script cleaner_area_onenter as OnEnter


//Gets the master area where oObject is in case of multi-area dungeon
// Returns oArea if there is no specific master
object Cleaner_GetMasterArea(object oObject){
	if(!GetIsObjectValid(oObject)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetMasterArea: Called Cleaner_GetMasterArea on invalid object " + _Cleaner_ObjectInfo(oObject));
		return OBJECT_INVALID;
	}
	if(GetIsObjectValid(GetLocalObject(oObject, "cleaner_area_master")))
		return GetLocalObject(oObject, "cleaner_area_master");

	object oObjArea = GetArea(oObject);
	if(oObjArea != oObject && GetIsObjectValid(oObjArea)){
		// oObject is an object in an area

		// Search for sub areas
		if(GetLocalInt(oObjArea, "cleaner_area_has_subareas")){
			object oSub = GetFirstSubArea(oObjArea, GetPosition(oObject));
			while(GetIsObjectValid(oSub)){
				if(GetIsObjectValid(GetLocalObject(oSub, "cleaner_area_master"))){
					object oMaster = GetLocalObject(oSub, "cleaner_area_master");
					if(GetObjectType(oObject) != OBJECT_TYPE_CREATURE)
						SetLocalObject(oObject, "cleaner_area_master", oMaster);
					if(!GetIsObjectValid(oMaster)){
						_Cleaner_SignalBug("Cleaner::Cleaner_GetMasterArea: cached cleaner_area_master linking to an invalid area");
						return OBJECT_INVALID;
					}
					return oMaster;
				}
				oSub = GetNextSubArea(oObjArea);
			}
		}
	}

	// Get master area
	object o;
	if(GetIsObjectValid(GetArea(oObject))) oObject = GetArea(oObject);
	while(GetIsObjectValid(o = GetLocalObject(oObject, "cleaner_area_master")))
		oObject = o;
	if(!GetIsObjectValid(oObject)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetMasterArea: cleaner_area_master linking to an invalid area");
		return OBJECT_INVALID;
	}
	return oObject;
}

// Returns TRUE if oArea is the master area of the dungeon. Areas that are not
// part of a dungeon are always master areas.
int Cleaner_GetIsMasterArea(object oArea){
	return GetIsObjectValid(GetLocalObject(oArea, "cleaner_area_master")) == FALSE;
}

// Adds a slave area linked to the master area
// Rarely used
void Cleaner_AddSlaveArea(object oMasterArea, object oSlaveArea){
	if(!Cleaner_GetIsMasterArea(oMasterArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_AddSlaveArea: "+_Cleaner_ObjectInfo(oMasterArea)+" is not a master area");
		return;
	}
	SetLocalObject(oSlaveArea, "cleaner_area_master", oMasterArea);

	int nCount = GetLocalInt(oMasterArea, "cleaner_area_slave_count");
	SetLocalObject(oMasterArea, "cleaner_area_slave_"+IntToString(nCount), oSlaveArea);
	SetLocalInt(oMasterArea, "cleaner_area_slave_count", nCount+1);
}

// Gets the number of slave areas
int Cleaner_GetSlaveAreaCount(object oMasterArea){
	if(!Cleaner_GetIsMasterArea(oMasterArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetSlaveAreaCount: "+_Cleaner_ObjectInfo(oMasterArea)+" is not a master area");
		return 0;
	}
	return GetLocalInt(oMasterArea, "cleaner_area_slave_count");
}
// Gets a slave area by index
object Cleaner_GetSlaveArea(object oMasterArea, int i){
	if(!Cleaner_GetIsMasterArea(oMasterArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetSlaveArea: "+_Cleaner_ObjectInfo(oMasterArea)+" is not a master area");
		return OBJECT_INVALID;
	}
	return GetLocalObject(oMasterArea, "cleaner_area_slave_"+IntToString(i));
}
int _Cleaner_GetIsPcInArea(object oArea, object oException=OBJECT_INVALID){
	int bIsTrigger = GetObjectType(oArea) == OBJECT_TYPE_TRIGGER;

	object oPC=GetFirstPC();
	while(GetIsObjectValid(oPC))
	{
		if((bIsTrigger ? GetIsInSubArea(oPC, oArea) : (GetArea(oPC) == oArea))
			&& oPC!=oException
			&& (!GetIsDM(oPC) || !GetLocalInt(oPC, "dm_is_invisible")) ){
			return TRUE;
		}
		oPC=GetNextPC();
	}
	return FALSE;
}

// Returns TRUE if there is any PC is in any area of the dungeon
//
// oException: Ignore this PC if it is in the dungeon (useful for OnAreaEnter scripts)
int Cleaner_GetIsPcInDungeon(object oArea, object oException=OBJECT_INVALID){
	oArea = Cleaner_GetMasterArea(oArea);

	if(_Cleaner_GetIsPcInArea(oArea, oException))
		return TRUE;

	int nSlaveCount = Cleaner_GetSlaveAreaCount(oArea);
	int i;
	for(i = 0 ; i < nSlaveCount ; i++){
		object oSlaveArea = Cleaner_GetSlaveArea(oArea, i);
		if(_Cleaner_GetIsPcInArea(oSlaveArea, oException))
			return TRUE;
	}
	return FALSE;
}

// Returns TRUE if oArea is a sub-area of the dungeon oDungeonArea
int Cleaner_GetIsAreaInDungeon(object oArea, object oDungeonArea){
	oDungeonArea = Cleaner_GetMasterArea(oDungeonArea);

	if(oArea == oDungeonArea)
		return TRUE;

	int nSlaveCount = Cleaner_GetSlaveAreaCount(oDungeonArea);
	int i;
	for(i = 0 ; i < nSlaveCount ; i++){
		object oSlaveArea = Cleaner_GetSlaveArea(oDungeonArea, i);
		if(oArea == oSlaveArea)
			return TRUE;
	}
	return FALSE;
}


//=============================================================================
// Variable management
//=============================================================================
//
// Those variables are reset once the area is cleaned
//

// Gets a local string variable on oAreaObject's area (or oAreaObject if it's
// an area) that will be automatically removed when the area is cleaned.
//
// sName: name of the variable
// oAreaObject: Cleaner area or any object that is in the cleaner area
string Cleaner_GetString(string sName, object oAreaObject=OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetString: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return "";
	}
	return GetLocalString(oArea, "cleaner_var_"+sName);
}
// See Cleaner_GetString
void Cleaner_SetString(string sName, string sValue, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_SetString: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return;
	}
	SetLocalString(oArea, "cleaner_var_"+sName, sValue);
}
// See Cleaner_GetString
int Cleaner_GetInt(string sName, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetInt: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return 0;
	}
	return GetLocalInt(oArea, "cleaner_var_"+sName);
}
// See Cleaner_GetString
void Cleaner_SetInt(string sName, int nValue, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_SetInt: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return;
	}
	SetLocalInt(oArea, "cleaner_var_"+sName, nValue);
}
// See Cleaner_GetString
float Cleaner_GetFloat(string sName, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetFloat: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return 0.0;
	}
	return GetLocalFloat(oArea, "cleaner_var_"+sName);
}
// See Cleaner_GetString
void Cleaner_SetFloat(string sName, float fValue, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_SetFloat: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return;
	}
	SetLocalFloat(oArea, "cleaner_var_"+sName, fValue);
}
// See Cleaner_GetString
object Cleaner_GetObject(string sName, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetObject: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return OBJECT_INVALID;
	}
	return GetLocalObject(oArea, "cleaner_var_"+sName);
}
// See Cleaner_GetString
void Cleaner_SetObject(string sName, object oValue, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_SetObject: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return;
	}
	SetLocalObject(oArea, "cleaner_var_"+sName, oValue);
}
// See Cleaner_GetString
location Cleaner_GetLocation(string sName, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_GetLocation: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return Location(OBJECT_INVALID, [0.0,0.0,0.0], 0.0);
	}
	return GetLocalLocation(oArea, "cleaner_var_"+sName);
}
// See Cleaner_GetString
void Cleaner_SetLocation(string sName, location lValue, object oAreaObject = OBJECT_SELF){
	object oArea = Cleaner_GetMasterArea(oAreaObject);
	if(!GetIsObjectValid(oArea)){
		_Cleaner_SignalBug("Cleaner::Cleaner_SetLocation: Could not get area from object "+_Cleaner_ObjectInfo(oAreaObject));
		return;
	}
	SetLocalLocation(oArea, "cleaner_var_"+sName, lValue);
}


// Activable* functions helps with stuff that can be fired only once, until
// they are reset manually, on a timer or when the area is cleaned.
// ActivableActive returns TRUE if the activable is currently active.
//
// sName: name of the activable
// oAreaObject: Cleaner area or any object that is in the cleaner area
int Cleaner_ActivableActive(string sName, object oAreaObject = OBJECT_SELF){
	return Cleaner_GetInt(sName, oAreaObject);
}

// Activable* functions helps with stuff that can be fired only once, until
// they are reset manually, on a timer or when the area is cleaned.
// ActivableReset tries to reset the activable and returns TRUE if it has
// been successfully reset.
//
// sName: name of the activable
// oAreaObject: Cleaner area or any object that is in the cleaner area
int Cleaner_ActivableReset(string sName, object oAreaObject = OBJECT_SELF){
	if(Cleaner_ActivableActive(sName, oAreaObject)){
		Cleaner_SetInt(sName, FALSE, oAreaObject);
		return TRUE;
	}
	return FALSE;
}


// See Cleaner_ActivableReset
void Cleaner_ActivableResetVoid(string sName, object oAreaObject = OBJECT_SELF){
	Cleaner_ActivableReset(sName, oAreaObject);
}

// Activable* functions helps with stuff that can be fired only once, until
// they are reset manually, on a timer or when the area is cleaned.
// ActivableFire tries to trigger the activable and returns TRUE if it has
// been successfully fired.
//
// sName: name of the activable
// fDuration: Delay before the activable is automatically reset (negative to never reset)
// oAreaObject: Cleaner area or any object that is in the cleaner area
int Cleaner_ActivableFire(string sName, float fDuration=-1.0, object oAreaObject = OBJECT_SELF){
	if(!Cleaner_ActivableActive(sName, oAreaObject)){
		Cleaner_SetInt(sName, TRUE, oAreaObject);
		if(fDuration >= 0.0)
			AssignCommand(oAreaObject, DelayCommand(fDuration, Cleaner_ActivableResetVoid(sName, oAreaObject)));
		return TRUE;
	}
	return FALSE;
}

// See Cleaner_ActivableFire
void Cleaner_ActivableFireVoid(string sName, float fDuration=-1.0, object oAreaObject = OBJECT_SELF){
	Cleaner_ActivableFire(sName, fDuration, oAreaObject);
}



//=============================================================================
// Run ID
//=============================================================================
//
// Each time the dungeon is cleaned, a new RunID is generated
//

// Gets an unique identifier that is reset each time the dungeon is cleaned.
// The number is generated if the local variable cleaner_runid on the area is 0
// You can set your own cleaner_runid for multi-area dungeons (generate one for the first area, replicate value on other areas)
int Cleaner_GetDungeonRunId(object oArea){
	oArea = Cleaner_GetMasterArea(oArea);
	return GetLocalInt(oArea, "cleaner_runid");
}





//=============================================================================
// Object management
//=============================================================================


//Executes an action on oObject when oObject's area is cleaned
// WARNING: only works on object that are placed directly on the map (placeables, creatures, placed effects, ...)
void Cleaner_SetObjectOnCleanAction(object oObject, string sAction){
	SetLocalString(oObject, "cleaner_action", sAction);
}

//Execute an action on oItem when the player enters a new area and oArea has been cleaned
// The action will only be executed once, whether or not sAction destroys the item
// Note: Based on the run ID, il you want the item to be removed on a multi-area dungeon,
//       be sure that the runId is the same on all areas
//
// oItem: item to execute sAction on
// oArea: Area to link the item with
// sAction: See Cleaner_DoActionOnObject
// bChangeName: TRUE to change the name of the item, so players know it will be destroyed/cleaned on exit
void Cleaner_RegisterItemOnCleanAction(object oItem, object oArea, string sAction, int bChangeName=TRUE){
	if(GetObjectType(oItem) != OBJECT_TYPE_ITEM){
		_Cleaner_SignalBug("Cleaner::Cleaner_RegisterItemOnCleanAction called on "+_Cleaner_ObjectInfo(oItem)+" which is not an item");
		return;
	}
	if(StringToInt(Get2DAString("baseitems", "Stacking", GetBaseItemType(oItem))) > 1){
		_Cleaner_SignalBug("Cleaner: Warning: Cleaner_RegisterItemOnCleanAction called on "+_Cleaner_ObjectInfo(oItem)+" which is stackable (can cause the entire stack destruction on cleanup, or prevent cleanup if stack is split)");
	}

	object oMasterArea = Cleaner_GetMasterArea(oArea);
	int nRunId = Cleaner_GetDungeonRunId(oMasterArea);
	SetLocalObject(oItem, "cleaner_area", oMasterArea);
	SetLocalInt(oItem, "cleaner_runid", nRunId);
	SetLocalString(oItem, "cleaner_action", sAction);

	SetLocalObject(oMasterArea, "cleaner_item#"+IntToString(ObjectToInt(oItem)), oItem);

	//Modif description item:
	if(bChangeName){
		SetFirstName(oItem, GetName(oItem)+"\n<i>("+GetName(oArea)+")</i>");
	}
}


// Returns the area that the item has been registered to. Returns
// OBJECT_INVALID if the item has not been registered or if the area run ID
// has changed.
object Cleaner_GetItemRegisteredArea(object oItem){
	object oArea = GetLocalObject(oItem, "cleaner_area");
	if(GetLocalInt(oItem, "cleaner_runid") == Cleaner_GetDungeonRunId(oArea))
		return GetLocalObject(oItem, "cleaner_area");
	return OBJECT_INVALID;
}


//Execute an action on oObject when oArea is cleaned.
// oObject can be in another area than oArea
//
// oArea: area that will trigger oObject's cleaning actions
// oObject: object that will be cleaned
// sAction: actions to perform on cleaning
// bExecOnce: if TRUE, the actions will be only executed one time. If FALSE,
//            the actions will be executed every time oArea is cleaned.
void Cleaner_RegisterObjectOnCleanAction(object oArea, object oObject, string sAction, int bExecOnce=TRUE){
	oArea = Cleaner_GetMasterArea(oArea);

	if(!GetIsObjectValid(oObject)){
		_Cleaner_SignalBug("Cleaner_RegisterObjectOnCleanAction: Invalid oObject");
		return;
	}

	if(bExecOnce)
		SetLocalString(oArea, "cleaner_once_obj#"+IntToString(ObjectToInt(oObject)), sAction);
	else
		SetLocalString(oArea, "cleaner_obj#"+IntToString(ObjectToInt(oObject)), sAction);
	SetLocalObject(oObject, "cleaner_area_master", oArea);
}

//Destroys and re-creates an object on the map (placeable, item, ...) when the
// area is cleaned. This function has to be called only once (typically in the
// MODULE_INIT event). Note that the object must be non-static, and the placed
// object must have a valid model (resref).
//
// The following properties are saved and restored:
//   - Model (resref)
//   - Tag
//   - Objet type
//   - Location (position + facing)
//   - Scale
//   - Local variables
//
// Params
//   oObject: object to register (generally a placeable)
//   oArea: Area that triggers this object's refresh. OBJECT_INVALID to use oObject's area
// Return
//   A string handle to use for Cleaner_RefreshObjectNow and Cleaner_GetRefreshableObject
string Cleaner_RegisterObjectToRefresh(object oObject, object oArea=OBJECT_INVALID){
	oArea = Cleaner_GetMasterArea(oArea == OBJECT_INVALID? oObject : oArea);

	if(!GetIsObjectValid(oObject)){
		_Cleaner_SignalBug("Cleaner_RegisterObjectToRefresh: Invalid oObject");
		return "";
	}

	string sHandle = IntToString(ObjectToInt(oObject));
	string sVar = "cleaner_refresh#"+sHandle;

	SetLocalObject(oArea, sVar, oObject);
	SetLocalString(oArea, sVar+"_model", GetResRef(oObject));
	SetLocalString(oArea, sVar+"_tag", GetTag(oObject));
	SetLocalInt(oArea, sVar+"_type", GetObjectType(oObject));
	SetLocalLocation(oArea, sVar+"_loc", GetLocation(oObject));
	SetLocalLocation(oArea, sVar+"_scale", Location(OBJECT_INVALID, Vector(GetScale(oObject, SCALE_X), GetScale(oObject, SCALE_Y), GetScale(oObject, SCALE_Z)), 0.0));

	int nVars = GetVariableCount(oObject);
	int i;
	for(i = 0 ; i < nVars ; i++){
		string sName = sVar + "_var_"+GetVariableName(oObject, i);
		switch(GetVariableType(oObject, i)){
			case VARIABLE_TYPE_INT:
				SetLocalInt(oArea, sName, GetVariableValueInt(oObject, i));
				break;
			case VARIABLE_TYPE_FLOAT:
				SetLocalFloat(oArea, sName, GetVariableValueFloat(oObject, i));
				break;
			case VARIABLE_TYPE_STRING:
				SetLocalString(oArea, sName, GetVariableValueString(oObject, i));
				break;
			case VARIABLE_TYPE_DWORD:
				SetLocalObject(oArea, sName, GetVariableValueObject(oObject, i));
				break;
			case VARIABLE_TYPE_LOCATION:
				SetLocalLocation(oArea, sName, GetVariableValueLocation(oObject, i));
				break;
		}
	}

	SetLocalString(oObject, "cleaner_refresh_handle", sHandle);
	return sHandle;
}
// Returns the refresh handle of an object that has been registered for
// refreshing with Cleaner_RegisterObjectToRefresh. "" if the object has not
// been registered.
string Cleaner_GetRefreshHandle(object oObject){
	return GetLocalString(oObject, "cleaner_refresh_handle");
}

// Returns TRUE if oObject has been registered for refreshing with
// Cleaner_RegisterObjectToRefresh
int Cleaner_GetIsRefreshRegistered(object oObject){
	return GetLocalString(oObject, "cleaner_refresh_handle") != "";
}

// Returns an object that has been registered for refreshing, using a string
// handle. If the object has been destroyed the returned object will be
// invalid.
//
// oArea: Area on which the object has been registered to
// sHandle: string handle as returned by Cleaner_RegisterObjectToRefresh
object Cleaner_GetRefreshableObject(object oArea, string sHandle){
	return GetLocalObject(Cleaner_GetMasterArea(oArea), "cleaner_refresh#"+sHandle);
}

// Refresh immediately a registered object, using a string handle (destroy any
// existing intance and create a new one).
// Returns the newly created object
object Cleaner_RefreshObjectNow(object oArea, string sHandle){
	oArea = Cleaner_GetMasterArea(oArea);

	string sHandleVar = "cleaner_refresh#"+sHandle;

	if(GetLocalString(oArea, sHandleVar+"_model") == ""){
		_Cleaner_SignalBug("Cleaner::Cleaner_RefreshObjectNow: sHandle='"+sHandle+"' does not exist in dungeon " + _Cleaner_ObjectInfo(oArea));
		return OBJECT_INVALID;
	}

	object oObject = GetLocalObject(oArea, "cleaner_refresh#"+sHandle);
	if(GetIsObjectValid(oObject))
		DestroyObject(oObject);
	oObject = CreateObject(
		GetLocalInt(oArea, sHandleVar+"_type"),
		GetLocalString(oArea, sHandleVar+"_model"),
		GetLocalLocation(oArea, sHandleVar+"_loc"),
		FALSE,
		GetLocalString(oArea, sHandleVar+"_tag"));
	if(!GetIsObjectValid(oObject)){
		_Cleaner_SignalBug("Cleaner::Cleaner_RefreshObjectNow: Could not re-create the placeable ! resref='"+GetLocalString(oArea, sHandleVar+"_model")+"' location="+LocationToString(GetLocalLocation(oArea, sHandleVar+"_loc")));
		return OBJECT_INVALID;
	}
	vector vScale = GetPositionFromLocation(GetLocalLocation(oArea, sHandleVar+"_scale"));
	SetScale(oObject, vScale.x, vScale.y, vScale.z);
	SetLocalString(oObject, "cleaner_refresh_handle", sHandle);
	SetLocalObject(oArea, sHandleVar, oObject);

	//Copy vars
	string sLocVarPrefix = sHandleVar + "_var_";
	int nLocVarPrefixLength = GetStringLength(sLocVarPrefix);

	int i;
	int nAreaVarCount = GetVariableCount(oArea);
	for(i = 0 ; i < nAreaVarCount ; i++){
		string sAreaVarName = GetVariableName(oArea, i);
		if(GetStringLeft(sAreaVarName, nLocVarPrefixLength) == sLocVarPrefix){
			string sObjVarName = GetStringRight(sAreaVarName, GetStringLength(sAreaVarName) - nLocVarPrefixLength);
			switch(GetVariableType(oArea, i)){
				case VARIABLE_TYPE_INT:
					SetLocalInt(oObject, sObjVarName, GetVariableValueInt(oArea, i));
					break;
				case VARIABLE_TYPE_FLOAT:
					SetLocalFloat(oObject, sObjVarName, GetVariableValueFloat(oArea, i));
					break;
				case VARIABLE_TYPE_STRING:
					SetLocalString(oObject, sObjVarName, GetVariableValueString(oArea, i));
					break;
				case VARIABLE_TYPE_DWORD:
					SetLocalObject(oObject, sObjVarName, GetVariableValueObject(oArea, i));
					break;
				case VARIABLE_TYPE_LOCATION:
					SetLocalLocation(oObject, sObjVarName, GetVariableValueLocation(oArea, i));
					break;
			}
		}

	}

	return oObject;
}

// See Cleaner_RefreshObjectNow
void Cleaner_RefreshObjectNowVoid(object oArea, string sHandle){
	Cleaner_RefreshObjectNow(oArea, sHandle);
}

// Executes an action string on oObject
//
// sAction values:
// destroy                  Destroy the object
// forcedestroy             Destroy object, even those with SetIsDestroyable(FALSE)
// script=yourscript        Execute yourscript on the object
// event=42                 Trigger user defined event 42 on the object
// deletevar=yourvarname    Deletes the variable by name
// sef=+fx_yolo             Add/remove sef (+sef to add, -sef to remove)
// useable=1                Makes oObject usable (1) or not (0)
// light=1                  Switches the light on (1) / off(0)
// sound=1                  Switches a placed sound object on (1) / off(0). WARNING: you need to register this action with Cleaner_RegisterObjectOnCleanAction
// plot=1                   Set plot flag
// name=hello world         Set the object name. WARNING: name cannot contain ';'
// floatingtext=hello world Sends a FloatingTextStringOnCreature to the item owner. WARNING: message cannot contain ';'
//
// You can execute multiple actions using ';' between (no spaces allowed)
void Cleaner_DoActionOnObject(object oObject, string sAction){
	if(sAction == "")
		return;
	if(!GetIsObjectValid(oObject))
		return;

	int nOffset = 0;
	int nSepOffset = FindSubString(sAction, ";", nOffset);
	if(nSepOffset < 0)
		nSepOffset = GetStringLength(sAction);
	string sSingleAction = GetSubString(sAction, nOffset, nSepOffset - nOffset);

	while(sSingleAction != ""){
		int nEqPos = FindSubString(sSingleAction, "=");
		string sActName = GetSubString(sSingleAction, 0, nEqPos);
		string sActValue = nEqPos >= 0 ? GetSubString(sSingleAction, nEqPos + 1, -1) : "";

		if(sActName == "destroy")
			_Cleaner_DestroyObjectAndInventory(oObject);
		else if(sActName == "forcedestroy"){
			AssignCommand(oObject, SetIsDestroyable(TRUE, FALSE, FALSE));
			_Cleaner_DestroyObjectAndInventory(oObject);
		}
		else if(sActName == "script")
			ExecuteScript(sActValue, oObject);
		else if(sActName == "event")
			SignalEvent(oObject, EventUserDefined(StringToInt(sActValue)));
		else if(sActName == "deletevar"){
			DeleteLocalInt(oObject, sActValue);
			DeleteLocalFloat(oObject, sActValue);
			DeleteLocalString(oObject, sActValue);
			DeleteLocalObject(oObject, sActValue);
			DeleteLocalLocation(oObject, sActValue);
		}
		else if(sActName == "sef"){
			string sAdd = GetSubString(sActValue, 0, 1);
			string sSef = GetSubString(sActValue, 1, -1);

			RemoveSEFFromObject(oObject, sSef);
			if(sAdd == "+")
				ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectNWN2SpecialEffectFile(sSef), oObject);
			else if(sAdd != "-")
				_Cleaner_SignalBug("Cleaner::Cleaner_DoActionOnObject: sef action '"+sSingleAction+"' needs +/- to add/remove the SEF on "+_Cleaner_ObjectInfo(oObject));
		}
		else if(sActName == "useable"){
			if(GetObjectType(oObject) != OBJECT_TYPE_PLACEABLE)
				_Cleaner_SignalBug("Cleaner::Cleaner_DoActionOnObject: Executing action 'useable' on an object that is not a placeable: "+_Cleaner_ObjectInfo(oObject));
			SetUseableFlag(oObject, StringToInt(sActValue));
		}
		else if(sActName == "light"){
			if(GetObjectType(oObject) != OBJECT_TYPE_LIGHT)
				_Cleaner_SignalBug("Cleaner::Cleaner_DoActionOnObject: Executing action 'light' on an object that is not an light: "+_Cleaner_ObjectInfo(oObject));
			SetLightActive(oObject, StringToInt(sActValue));
		}
		else if(sActName == "sound"){
			int nState = StringToInt(sActValue);
			if(nState) SoundObjectPlay(oObject);
			else       SoundObjectStop(oObject);
		}
		else if(sActName == "plot")
			SetPlotFlag(oObject, StringToInt(sActValue));
		else if(sActName == "name")
			SetFirstName(oObject, sActValue);
		else if(sActName == "floatingtext"){
			object oTarget = oObject;
			if(GetObjectType(oObject) == OBJECT_TYPE_ITEM && GetIsObjectValid(GetItemPossessor(oObject)))
				oTarget = GetItemPossessor(oObject);
			FloatingTextStringOnCreature(sActValue, oTarget, FALSE);
		}
		else if(sActName == "encounter"){
			if(GetObjectType(oObject) != OBJECT_TYPE_TRIGGER)
				_Cleaner_SignalBug("Cleaner::Cleaner_DoActionOnObject: Executing action 'encounter' on an object that is not an encounter: "+_Cleaner_ObjectInfo(oObject));
			SetEncounterActive(StringToInt(sActValue), oObject);
		}
		else if(_Cleaner_DoExtraActionOnObject(oObject, sActName, sActValue) == FALSE)
			_Cleaner_SignalBug("Cleaner::Cleaner_DoActionOnObject: Unknown action: '"+sSingleAction+"' for object "+_Cleaner_ObjectInfo(oObject));

		//
		nOffset = nSepOffset+1;
		nSepOffset = FindSubString(sAction, ";", nOffset);
		if(nSepOffset < 0)
			nSepOffset = GetStringLength(sAction);
		sSingleAction = GetSubString(sAction, nOffset, nSepOffset - nOffset);
	}
}

string Cleaner_AppendAction(string sActionA, string sActionB){
	return sActionA == ""? sActionB : sActionA+";"+sActionB;
}


const int CLEANER_FLAG_KEEPSCRIPT = 1;//For creatures: Do not SetScriptHidden
const int CLEANER_FLAG_DONOTHEAL =  2;//For creatures, placeables & doors
const int CLEANER_FLAG_DONOTAUTODESTROY =  4;//For items on the floor & AOE objects
const int CLEANER_FLAG_DONOTRESTOREOPEN = 8;
const int CLEANER_FLAG_DONOTRESTORETRAP = 16;
const int CLEANER_FLAG_DONOTRESTORELOCK = 32;
const int CLEANER_FLAG_ALL = 0xFF;

//Set specific flags on oObject, while keeping other flag values
void Cleaner_AddFlags(object oObject, int flags, int bValue){
	int currentFlags = GetLocalInt(oObject, "cleaner_flags");
	if(bValue)
		currentFlags |= flags;
	else
		currentFlags &= flags ^ CLEANER_FLAG_ALL;
	SetLocalInt(oObject, "cleaner_flags", currentFlags);
}

//Set cleaner flags for oObject. See CLEANER_FLAG_*
void Cleaner_SetFlags(object oObject, int flags){
	SetLocalInt(oObject, "cleaner_flags", flags);
}

//Get cleaner flags for oObject. See CLEANER_FLAG_*
int Cleaner_GetFlags(object oObject){
	return GetLocalInt(oObject, "cleaner_flags");
}

//Restore musics as they were configured in the toolset
void Cleaner_RestoreAreaMusic(object oArea){
	MusicBackgroundChangeDay(oArea, GetLocalInt(oArea, "cleaner_music_day"));
	MusicBackgroundChangeNight(oArea, GetLocalInt(oArea, "cleaner_music_night"));
	MusicBattleChange(oArea, GetLocalInt(oArea, "cleaner_music_battle"));
}


// Try to clean immediately the dungeon (ignoring the delay).
//
// Returns TRUE if the area has been cleaned, or FALSE if there is a PC in the
// dungeon or in a transition from/to the dungeon.
int Cleaner_CleanDungeonNow(object oArea, int bForce = FALSE){
	object oMasterArea = Cleaner_GetMasterArea(oArea);
	if(!bForce){
		int bPlayersInDungeon = Cleaner_GetIsPcInDungeon(oMasterArea);
		if(bPlayersInDungeon || _Cleaner_Watchdog_GetHasCleanLocks(oMasterArea))
			return FALSE;
	}

	//Clean areas
	if(_CLEANER_DEBUG)
		_Cleaner_BroadcastMsg("<b><c=orange>============ CLEANING DUNGEON "+GetTag(oMasterArea)+" ===========</c></b>");

	_Cleaner_CleanSingleArea(oMasterArea);

	int nSlaveCount = Cleaner_GetSlaveAreaCount(oMasterArea);
	int i;
	for(i = 0 ; i < nSlaveCount ; i++){
		object oSlaveArea = Cleaner_GetSlaveArea(oMasterArea, i);
		DelayCommand(0.0, _Cleaner_CleanSingleArea(oSlaveArea));
	}
	DeleteLocalInt(oMasterArea, "cleaner_runid");
	return TRUE;
}

// Checks a player's inventory for items registered with
// Cleaner_RegisterItemOnCleanAction and execute cleaning actions if needed.
//
// Note: the cleaner_area_onenter already calls this function when needed.
// This function is only useful if you don't use cleaner_area_onenter in all
// your module areas.
void Cleaner_CheckInventory(object oPC){
	object oItem = GetFirstItemInInventory(oPC);
	while(GetIsObjectValid(oItem)){
		if(GetLocalInt(oItem, "cleaner_runid") > 0)
			_Cleaner_CheckItem(oItem);
		oItem = GetNextItemInInventory(oPC);
	}
	int i;
	for(i = 0 ; i < NUM_INVENTORY_SLOTS ; i++){
		object oItem = GetItemInSlot(i, oPC);
		if(GetLocalInt(oItem, "cleaner_runid") > 0)
			_Cleaner_CheckItem(oItem);
	}
}

//=============================================================================
// Internal functions (do not use)
//=============================================================================


void _Cleaner_ExecuteEventScript(object oArea, int nEvent){
	string sScript = GetLocalString(oArea, "cleaner_event_script");
	if(sScript != ""){
		ClearScriptParams();
		AddScriptParameterInt(nEvent);
		ExecuteScriptEnhanced(sScript, oArea);
	}
}

void _Cleaner_CheckItem(object oItem){
	int nItemRunId = GetLocalInt(oItem, "cleaner_runid");
	if(nItemRunId > 0){
		object oCleanerArea = GetLocalObject(oItem, "cleaner_area");
		if(!GetIsObjectValid(oCleanerArea) || nItemRunId != Cleaner_GetDungeonRunId(oCleanerArea)){
			string sAction = GetLocalString(oItem, "cleaner_action");
			Cleaner_DoActionOnObject(oItem, sAction);

			//Delete area reference
			DeleteLocalObject(oCleanerArea, "cleaner_item#"+IntToString(ObjectToInt(oItem)));

			//Delete item info
			DeleteLocalString(oItem, "cleaner_action");
			DeleteLocalString(oItem, "cleaner_runid");
			DeleteLocalString(oItem, "cleaner_area");
		}
		else{
			//Register item on area (disconnection changes inventory items ref.)
			SetLocalObject(oCleanerArea, "cleaner_item#"+IntToString(ObjectToInt(oItem)), oItem);
		}
	}
}

void _Cleaner_SaveAreaState(object oArea){

	SetLocalInt(oArea, "cleaner_music_day", MusicBackgroundGetDayTrack(oArea));
	SetLocalInt(oArea, "cleaner_music_night", MusicBackgroundGetNightTrack(oArea));
	SetLocalInt(oArea, "cleaner_music_battle", MusicBackgroundGetBattleTrack(oArea));

	location lCenter = Location(GetObjectType(oArea) == OBJECT_TYPE_TRIGGER ? GetArea(oArea) : oArea, Vector(), 0.0);
	object oObject = GetFirstObjectInShape(SHAPE_CUBE, 10000.0, lCenter, FALSE, OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_DOOR);
	while(GetIsObjectValid(oObject))
	{
		switch(GetObjectType(oObject))
		{
			//Enregistrement de l'état des portes/coffres
			case OBJECT_TYPE_DOOR:
				SetLocalInt(oObject, "cleaner_module_init_openstate", GetIsOpen(oObject));
				//Fallthrough
			case OBJECT_TYPE_PLACEABLE:
				if(GetIsTrapped(oObject)){
					SetLocalInt(oObject, "cleaner_module_init_trapped", TRUE);
					SetLocalInt(oObject, "cleaner_module_init_traptype", GetTrapBaseType(oObject));
				}
				SetLocalInt(oObject, "cleaner_module_init_lockstate", GetLocked(oObject));
				break;
		}
		oObject = GetNextObjectInShape(SHAPE_CUBE, 10000.0, lCenter, FALSE, OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_DOOR);
	}
}

void _Cleaner_InitSingleArea(object oArea){

	if(GetLocalInt(oArea, "cleaner_init") == FALSE){
		SetLocalInt(oArea, "cleaner_init", TRUE);

		if(_CLEANER_DEBUG)
			_Cleaner_BroadcastMsg("<b><c=orange>------------ "+GetTag(Cleaner_GetMasterArea(oArea))+": Init area "+GetTag(oArea)+"</c></b>");

		int bIsTrigger = GetObjectType(oArea) == OBJECT_TYPE_TRIGGER;

		location lCenter = Location(bIsTrigger ? GetArea(oArea) : oArea, Vector(), 0.0);

		object oCreature = GetFirstObjectInShape(SHAPE_CUBE, 10000.0, lCenter);
		while(GetIsObjectValid(oCreature))
		{
			if(bIsTrigger == FALSE || GetIsInSubArea(oCreature, oArea)){
				string sSuffix = GetStringRight(GetTag(oCreature), 4);
				if(sSuffix == "jour")
					SetScriptHidden(oCreature, !GetIsDay());
				else if(sSuffix == "nuit")
					SetScriptHidden(oCreature, GetIsDay());
				else
					SetScriptHidden(oCreature, FALSE);
			}
			oCreature = GetNextObjectInShape(SHAPE_CUBE, 10000.0, lCenter);
		}

		if(bIsTrigger){
			SetLocalInt(GetArea(oArea), "cleaner_area_has_subareas", TRUE);
		}

		_Cleaner_ExecuteEventScript(oArea, CLEANER_EVENT_INIT);
	}
}


const int _Cleaner_CleanSingleArea_flags = 91; // OBJECT_TYPE_CREATURE|OBJECT_TYPE_PLACEABLE|OBJECT_TYPE_DOOR|OBJECT_TYPE_ITEM|OBJECT_TYPE_AREA_OF_EFFECT
void _Cleaner_CleanSingleArea(object oArea){
	if(GetLocalInt(oArea, "cleaner_init") == TRUE){
		SetLocalInt(oArea, "cleaner_init", FALSE);

		if(_CLEANER_DEBUG)
			_Cleaner_BroadcastMsg("<b><c=orange>------------ "+GetTag(Cleaner_GetMasterArea(oArea))+": Clean area "+GetTag(oArea)+"</c></b>");

		_Cleaner_ExecuteEventScript(oArea, CLEANER_EVENT_CLEAN);

		if(Cleaner_GetIsMasterArea(oArea)){
			//Clean master area
			int nCount = GetVariableCount(oArea);
			int i;
			for(i = nCount ; i >= 0 ; i--){
				string sName = GetVariableName(oArea, i);
				if(GetStringLeft(sName, 8) == "cleaner_"){
					//Delete vars
					if(GetStringLeft(sName, 12) == "cleaner_var_" || GetStringLeft(sName, 13) == "cleaner_lock_"){
						switch(GetVariableType(oArea, i)){
							case VARIABLE_TYPE_INT:      DeleteLocalInt(oArea, sName);      break;
							case VARIABLE_TYPE_FLOAT:    DeleteLocalFloat(oArea, sName);    break;
							case VARIABLE_TYPE_STRING:   DeleteLocalString(oArea, sName);   break;
							case VARIABLE_TYPE_DWORD:    DeleteLocalObject(oArea, sName);   break;
							case VARIABLE_TYPE_LOCATION: DeleteLocalLocation(oArea, sName); break;
						}
					}
					//Refresh placeables
					else if(GetStringLeft(sName, 16) == "cleaner_refresh#"){
						int bMainVar = FindSubString(sName, "_", 16) < 0;
						if(bMainVar){
							string sHandle = GetStringRight(sName, GetStringLength(sName) - 16);

							Cleaner_RefreshObjectNow(oArea, sHandle);
						}
					}
					//Execute temporary actions
					else if(GetStringLeft(sName, 17) == "cleaner_once_obj#"){
						object oObject = StringToObject(GetStringRight(sName, GetStringLength(sName) - 17));
						if(GetIsObjectValid(oObject)){
							Cleaner_DoActionOnObject(oObject, GetVariableValueString(oArea, i));
							DeleteLocalObject(oObject, "cleaner_area_master");
						}
						DeleteLocalString(oArea, sName);
					}
					//Execute permanent actions
					else if(GetStringLeft(sName, 12) == "cleaner_obj#"){
						object oObject = StringToObject(GetStringRight(sName, GetStringLength(sName) - 12));
						if(GetIsObjectValid(oObject))
							Cleaner_DoActionOnObject(oObject, GetVariableValueString(oArea, i));
					}
					//Execute item actions
					else if(GetStringLeft(sName, 13) == "cleaner_item#"){
						object oItem = GetVariableValueObject(oArea, i);
						if(GetIsObjectValid(oItem)){
							Cleaner_DoActionOnObject(oItem, GetLocalString(oItem, "cleaner_action"));

							//Delete item info
							DeleteLocalString(oItem, "cleaner_action");
							DeleteLocalString(oItem, "cleaner_runid");
							DeleteLocalString(oItem, "cleaner_area");
						}

						//Delete area reference
						DeleteLocalObject(oArea, sName);
					}
				}
			}
		}

		int bIsTrigger = GetObjectType(oArea) == OBJECT_TYPE_TRIGGER;

		if(!bIsTrigger){
			// Clean area properties
			Cleaner_RestoreAreaMusic(oArea);
		}

		//Clean area objects
		object oRealArea = GetArea(oArea);// oArea can be a trigger
		object oObject = GetFirstObjectInArea(oRealArea);
		while(GetIsObjectValid(oObject))
		{
			if(bIsTrigger == FALSE || GetIsInSubArea(oObject, oArea)){
				int flags = GetLocalInt(oObject, "cleaner_flags");

				switch(GetObjectType(oObject)){
					case OBJECT_TYPE_CREATURE:{
						if(GetIsEncounterCreature(oObject))
							_Cleaner_DestroyObjectAndInventory(oObject);

						//Heal
						if((flags & CLEANER_FLAG_DONOTHEAL) == 0)
							ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oObject)), oObject);

						if((flags & CLEANER_FLAG_KEEPSCRIPT) == 0)
							SetScriptHidden(oObject, TRUE);
					}break;

					case OBJECT_TYPE_PLACEABLE:{
						string sTag = GetTag(oObject);
						if(sTag == "BodyBag" || (GetStringLength(sTag) == 8 && GetStringLeft(sTag, 7) == "LootBag"))
							_Cleaner_DestroyObjectAndInventory(oObject);

						//Restore trap
						if((flags & CLEANER_FLAG_DONOTRESTORETRAP) == 0 && GetLocalInt(oObject, "cleaner_module_init_trapped")){
							SetTrapDisabled(oObject);
							int nTrap = GetLocalInt(oObject, "cleaner_module_init_traptype");
							DelayCommand(0.1, CreateTrapOnObject(nTrap, oObject));
						}

						//Restore lock
						if((flags & CLEANER_FLAG_DONOTRESTORELOCK) == 0)
							SetLocked(oObject, GetLocalInt(oObject, "cleaner_module_init_lockstate"));

						//Heal
						if((flags & CLEANER_FLAG_DONOTHEAL) == 0)
							ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oObject)), oObject);
					}break;

					case OBJECT_TYPE_DOOR:{
						//Restore open/close state
						if((flags & CLEANER_FLAG_DONOTRESTOREOPEN) == 0){
							if(GetLocalInt(oObject, "cleaner_module_init_openstate"))
								AssignCommand(oObject, ActionOpenDoor(oObject));
							else
								AssignCommand(oObject, ActionCloseDoor(oObject));
						}

						//Restore trap
						if((flags & CLEANER_FLAG_DONOTRESTORETRAP) == 0 && GetLocalInt(oObject, "cleaner_module_init_trapped")){
							SetTrapDisabled(oObject);
							int nTrap = GetLocalInt(oObject, "cleaner_module_init_traptype");
							DelayCommand(0.1, CreateTrapOnObject(nTrap, oObject));
						}

						//Restore lock
						if((flags & CLEANER_FLAG_DONOTRESTORELOCK) == 0)
							SetLocked(oObject, GetLocalInt(oObject, "cleaner_module_init_lockstate"));

						//Heal
						if((flags & CLEANER_FLAG_DONOTHEAL) == 0)
							ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oObject)), oObject);
					}break;

					case OBJECT_TYPE_ITEM:{
						if((flags & CLEANER_FLAG_DONOTAUTODESTROY) == 0){
							_Cleaner_DestroyObjectAndInventory(oObject);
						}
					}break;

					case OBJECT_TYPE_AREA_OF_EFFECT:{
						if((flags & CLEANER_FLAG_DONOTAUTODESTROY) == 0)
							DestroyObject(oObject);
					}break;
				}

				//Execute registered action
				string sAction = GetLocalString(oObject, "cleaner_action");
				if(sAction != ""){
					Cleaner_DoActionOnObject(oObject, sAction);
				}

			}

			oObject = GetNextObjectInArea(oRealArea);
		}
	}
}

int _Cleaner_Watchdog_GetHasCleanLocks(object oMasterArea){
	int nVarCount = GetVariableCount(oMasterArea);
	int i;
	for(i = 0 ; i < nVarCount ; i++){
		if(GetStringLeft(GetVariableName(oMasterArea, i), 13) == "cleaner_lock_")
			return TRUE;
	}
	return FALSE;
}

void _Cleaner_Watchdog(int nRunID, int _emptyCounter = 0){
	object oMasterArea = OBJECT_SELF;
	if(Cleaner_GetDungeonRunId(oMasterArea) != nRunID)
		return;

	if(_CLEANER_DEBUG)
		_Cleaner_BroadcastMsg("watchdog " + GetTag(oMasterArea) + " empty_counter=" + IntToString(_emptyCounter));

	int bPlayersInDungeon = Cleaner_GetIsPcInDungeon(oMasterArea);

	if(bPlayersInDungeon == FALSE
	&& _Cleaner_Watchdog_GetHasCleanLocks(oMasterArea) == FALSE
	&& _emptyCounter >= _CLEANER_AREA_CLEANUP_DELAY_ROUNDS){
		//Clean areas
		Cleaner_CleanDungeonNow(oMasterArea, TRUE);
	}
	else
		DelayCommand(6.0, _Cleaner_Watchdog(nRunID, bPlayersInDungeon ? 0 : _emptyCounter + 1));

}


void _Cleaner_PauseArea(object oArea){
	if(GetObjectType(oArea) == OBJECT_TYPE_TRIGGER)
		oArea = GetArea(oArea);

	if(_CLEANER_DEBUG)
		_Cleaner_BroadcastMsg("<b><c=orange>............ "+GetTag(Cleaner_GetMasterArea(oArea))+": Pause area "+GetTag(oArea)+"</c></b>");

	location lCenter = Location(oArea, Vector(), 0.0);
	object oCreature = GetFirstObjectInShape(SHAPE_CUBE, 10000.0, lCenter);
	while(GetIsObjectValid(oCreature))
	{
		int flags = GetLocalInt(oCreature, "cleaner_flags");
		if((flags & CLEANER_FLAG_KEEPSCRIPT) == 0)
			SetScriptHidden(oCreature, TRUE);
		oCreature = GetNextObjectInShape(SHAPE_CUBE, 10000.0, lCenter);
	}
}

void _Cleaner_ResumeArea(object oArea){
	if(GetObjectType(oArea) == OBJECT_TYPE_TRIGGER)
		oArea = GetArea(oArea);

	if(_CLEANER_DEBUG)
		_Cleaner_BroadcastMsg("<b><c=orange>............ "+GetTag(Cleaner_GetMasterArea(oArea))+": Resume area "+GetTag(oArea)+"</c></b>");

	location lCenter = Location(oArea, Vector(), 0.0);
	object oCreature = GetFirstObjectInShape(SHAPE_CUBE, 10000.0, lCenter);
	while(GetIsObjectValid(oCreature))
	{
		int flags = GetLocalInt(oCreature, "cleaner_flags");
		if((flags & CLEANER_FLAG_KEEPSCRIPT) == 0)
			SetScriptHidden(oCreature, FALSE);
		oCreature = GetNextObjectInShape(SHAPE_CUBE, 10000.0, lCenter);
	}
}

string _Cleaner_ObjectInfo(object o)
{
	if(!GetIsObjectValid(o))//Is invalid
		return "OBJECT_INVALID (id:"+ObjectToString(o)+")";

	if(GetIsPC(o))//Is a player
		return "'"+GetName(o)+"' (Account:'"+GetPCPlayerName(o)+"' Area["+_Cleaner_ObjectInfo(GetArea(o))+"])";
	else if(o == GetArea(o))//Is an area
		return "'"+GetName(o)+"' (Tag:'"+GetTag(o)+"')";

	switch(GetObjectType(o))
	{
		case OBJECT_TYPE_ITEM:
		{
			object oOwner = GetItemPossessor(o);
			return "'"+GetName(o)+"' (Tag:'"+GetTag(o)+"' Resref:'"+GetResRef(o)+"' "+(GetIsObjectValid(oOwner) ? "Owner:"+_Cleaner_ObjectInfo(oOwner) : "") + ")";
		}
	}

	return "'"+GetName(o)+"' (Tag:'"+GetTag(o)+"' Area["+_Cleaner_ObjectInfo(GetArea(o))+"])";
}
void _Cleaner_DestroyObjectAndInventory(object oObject, float fDelay=0.0){
	object oItem = GetFirstItemInInventory(oObject);
	while(GetIsObjectValid(oItem))
	{
		DestroyObject(oItem, fDelay);
		oItem = GetNextItemInInventory(oObject);
	}
	DestroyObject(oObject, fDelay);
}
void _Cleaner_BroadcastMsg(string sMessage){
	object oPC = GetFirstPC();
	while(GetIsObjectValid(oPC))
	{
		SendMessageToPC(oPC, sMessage);
		oPC = GetNextPC();
	}
}
