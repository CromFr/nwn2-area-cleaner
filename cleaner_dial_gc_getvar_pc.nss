// Condition on a cleaner variable that is tied to the player.
// Internally, this will use a cleaner variable named <sPrefix><pc_object_id>.
//
// sType: int, float, string
// sPrefix: variable name
// sCheck:
//     >=5
//     ==42
//     ==somestring
//     =!somestring


#include "cleaner_inc"

string CheckStringNumber(string s, int bAllowFloat){
	int nLength = GetStringLength(s);
	int i;
	for(i = 0 ; i < nLength ; i++)
	{
		string c = GetSubString(s, i, 1);
		if(c != "0"
		&& c != "1"
		&& c != "2"
		&& c != "3"
		&& c != "4"
		&& c != "5"
		&& c != "6"
		&& c != "7"
		&& c != "8"
		&& c != "9"
		&& (bAllowFloat? c != "." : TRUE) ){
			_Cleaner_SignalBug("cleaner_dial_gc_getvar_pc: The string '"+s+"' is not a number (in dialog with "+ _Cleaner_ObjectInfo(OBJECT_SELF)+")");
			return s;
		}
	}
	return s;
}

int StartingConditional(string sType, string sPrefix, string sCheck)
{
	object oNPC = OBJECT_SELF;
	object oPC = GetPCSpeaker();

	string sName = sPrefix + ObjectToString(oPC);

	int nType;
	if(sType == "int"){
		int nLength = GetStringLength(sCheck);
		string sCheck1 = GetStringLeft(sCheck, 1);
		string sCheck2 = GetStringLeft(sCheck, 2);

		if(sCheck2 == "==")
			return Cleaner_GetInt(sName) == StringToInt(CheckStringNumber(GetStringRight(sCheck, nLength-2), FALSE));
		if(sCheck2 == "!=")
			return Cleaner_GetInt(sName) != StringToInt(CheckStringNumber(GetStringRight(sCheck, nLength-2), FALSE));
		else if(sCheck2 == "<=")
			return Cleaner_GetInt(sName) <= StringToInt(CheckStringNumber(GetStringRight(sCheck, nLength-2), FALSE));
		else if(sCheck2 == ">=")
			return Cleaner_GetInt(sName) >= StringToInt(CheckStringNumber(GetStringRight(sCheck, nLength-2), FALSE));
		else if(sCheck1 == "<")
			return Cleaner_GetInt(sName) < StringToInt(CheckStringNumber(GetStringRight(sCheck, nLength-1), FALSE));
		else if(sCheck1 == ">")
			return Cleaner_GetInt(sName) > StringToInt(CheckStringNumber(GetStringRight(sCheck, nLength-1), FALSE));
		else
			return Cleaner_GetInt(sName) == StringToInt(CheckStringNumber(GetStringRight(sCheck, nLength-2), FALSE));
	}
	else if(sType == "float"){
		int nLength = GetStringLength(sCheck);
		string sCheck1 = GetStringLeft(sCheck, 1);
		string sCheck2 = GetStringLeft(sCheck, 2);

		if(sCheck2 == "==")
			return Cleaner_GetFloat(sName) == StringToFloat(CheckStringNumber(GetStringRight(sCheck, nLength-2), TRUE));
		if(sCheck2 == "!=")
			return Cleaner_GetFloat(sName) != StringToFloat(CheckStringNumber(GetStringRight(sCheck, nLength-2), TRUE));
		else if(sCheck2 == "<=")
			return Cleaner_GetFloat(sName) <= StringToFloat(CheckStringNumber(GetStringRight(sCheck, nLength-2), TRUE));
		else if(sCheck2 == ">=")
			return Cleaner_GetFloat(sName) >= StringToFloat(CheckStringNumber(GetStringRight(sCheck, nLength-2), TRUE));
		else if(sCheck1 == "<")
			return Cleaner_GetFloat(sName) < StringToFloat(CheckStringNumber(GetStringRight(sCheck, nLength-1), TRUE));
		else if(sCheck1 == ">")
			return Cleaner_GetFloat(sName) > StringToFloat(CheckStringNumber(GetStringRight(sCheck, nLength-1), TRUE));
		else
			return Cleaner_GetFloat(sName) == StringToFloat(CheckStringNumber(GetStringRight(sCheck, nLength-2), TRUE));
	}
	else if(sType == "string"){
		int nLength = GetStringLength(sCheck);
		string sCheck1 = GetStringLeft(sCheck, 1);
		string sCheck2 = GetStringLeft(sCheck, 2);

		if(sCheck2 == "==")
			return Cleaner_GetString(sName) == GetStringRight(sCheck, nLength-2);
		if(sCheck2 == "!=")
			return Cleaner_GetString(sName) != GetStringRight(sCheck, nLength-2);
		else if(sCheck2 == "<=")
			return StringCompare(Cleaner_GetString(sName), GetStringRight(sCheck, nLength-2)) <= 0;
		else if(sCheck2 == ">=")
			return StringCompare(Cleaner_GetString(sName), GetStringRight(sCheck, nLength-2)) >= 0;
		else if(sCheck1 == "<")
			return StringCompare(Cleaner_GetString(sName), GetStringRight(sCheck, nLength-1)) < 0;
		else if(sCheck1 == ">")
			return StringCompare(Cleaner_GetString(sName), GetStringRight(sCheck, nLength-1)) > 0;
		else
			return Cleaner_GetString(sName) == GetStringRight(sCheck, nLength-2);

	}
	else
		_Cleaner_SignalBug("cleaner_dial_gc_getvar_pc: Invalid variable type '"+sType+"' (in dialog with "+_Cleaner_ObjectInfo(oNPC)+")");
	return FALSE;
}