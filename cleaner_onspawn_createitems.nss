// Creates one or more items tied to the dungeon, on a creature that is spawned
//
// Local variables:
// cleaner_items (string): the list of items to create, separated with | characters.
//   Each element of the list can be of the following form:
//   - <item_resref>: creates an item that will be destroyed on area cleaning
//     or dungeon exit
//   - <item_resref>:<action_list>: creates an item and perform action_list
//     actions on area cleaning or dungeon exit. See
//     cleaner_inc@Cleaner_DoActionOnObject for more information on
//     action_list
// OnSpawn (string): This script will be called when the creature is spawned.
//     Default to nw_c2_default9. Set to "none" to prevent the default spawn
//     script to be executed
//

#include "cleaner_inc"
#include "cleaner_inc_array"

void main()
{
	string sScript = GetLocalString(OBJECT_SELF, "OnSpawn");
	if(sScript != "none"){
		if(sScript != "")
			ExecuteScript(sScript, OBJECT_SELF);
		else
			ExecuteScript("nw_c2_default9", OBJECT_SELF);
	}

	struct array aItems = Array(GetLocalString(OBJECT_SELF, "cleaner_items"));
	int nSize = ArrayGetLength(aItems);
	int i;
	for(i = 0 ; i < nSize ; i++)
	{
		string sValue = ArrayGetString(aItems, i);
		string sAction = "destroy";
		string sResref;

		int nActionSepIdx;
		if((nActionSepIdx = FindSubString(sValue, ":")) >= 0){
			sResref = GetSubString(sValue, 0, nActionSepIdx);
			sAction = GetSubString(sValue, nActionSepIdx+1, GetStringLength(sValue) - (nActionSepIdx+1));
		}
		else{
			sResref = sValue;
		}

		object oItem = CreateItemOnObject(sResref, OBJECT_SELF);
		if(!GetIsObjectValid(oItem))
			_Cleaner_SignalBug("cleaner_onspawn_createitems: Cannot create item resref '" + sResref + "'");
		SetDroppableFlag(oItem, TRUE);
		Cleaner_RegisterItemOnCleanAction(oItem, GetArea(OBJECT_SELF), sAction);
	}
}