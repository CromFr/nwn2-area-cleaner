// Sets a cleaner variable
//
// sType: int, float, string
// sName: variable name
// sValue: variable value. The value will be converted to the appropriate type

#include "cleaner_inc"

void main(string sType, string sName, string sValue)
{
	object oNPC = OBJECT_SELF;

	int nType;
	if(sType == "int")         Cleaner_SetInt(sName, StringToInt(sValue));
	else if(sType == "float")  Cleaner_SetFloat(sName, StringToFloat(sValue));
	else if(sType == "string") Cleaner_SetString(sName, sValue);
	else _Cleaner_SignalBug("cleaner_dial_ga_setvar: Invalid variable type '"+sType+"' (in dialog with "+_Cleaner_ObjectInfo(oNPC)+")");
}