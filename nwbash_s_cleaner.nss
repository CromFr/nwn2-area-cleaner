#include "nwbash_inc"
#include "cleaner_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: cleaner <subcommand> [options]\n"
			+ "subcommand can be:\n"
			+ "info: Prints general information about the area and its config\n"
			+ "clean: Force clean the area\n"
			+ "export-master: exports the master area into a NWBash env var"
			+ "export-slave: exports one of the slave areas into a NWBash env var"
		);
		return 0;
	}
	// int bForce = NWBash_GetBooleanArg(args, "f|force");

	int argsLen = ArrayGetLength(args);
	if(argsLen < 1){
		NWBash_Err("Bad number of arguments / unknown options. See destroy --help");
		return 1;
	}

	string sSubCommand = ArrayGetString(args, 0);

	if(sSubCommand == "info"){
		int bHelp = NWBash_GetBooleanArg(args, "h|help");
		if(bHelp){
			NWBash_Info("Usage: cleaner info");
			NWBash_Info("");
			NWBash_Info("options can be:");
			NWBash_Info("-f, --force  Clean the area even if there is players inside");
			return 0;
		}

		object oTarget = NWBash_GetTarget();

		object oArea = GetArea(oTarget);
		object oMasterArea = Cleaner_GetMasterArea(oTarget);

		if(oTarget != oArea){
			NWBash_Info("Object: " + _Cleaner_ObjectInfo(oTarget));
			NWBash_Info("OnClean actions: " + GetLocalString(oTarget, "cleaner_action"));

			string sRegAction = GetLocalString(oMasterArea, "cleaner_once_obj#"+IntToString(ObjectToInt(oTarget)));
			if(sRegAction == "")
				sRegAction = GetLocalString(oMasterArea, "cleaner_obj#"+IntToString(ObjectToInt(oTarget)));
			if(sRegAction != "")
				NWBash_Info("Registered for cleaning: action=" + sRegAction);

			if(Cleaner_GetIsRefreshRegistered(oTarget))
				NWBash_Info("Registered for refreshing: handle=" + Cleaner_GetRefreshHandle(oTarget));

			if(GetObjectType(oTarget) == OBJECT_TYPE_ITEM){
				if(GetLocalInt(oTarget, "cleaner_runid") != 0){
					object oItemArea = GetLocalObject(oTarget, "cleaner_area");
					NWBash_Info("Registered as item for cleaning:");
					NWBash_Info("  dungeon: " + _Cleaner_ObjectInfo(oItemArea));
					NWBash_Info("  runid: " + IntToString(GetLocalInt(oTarget, "cleaner_runid")) + "(vs dungeon runid=" + IntToString(Cleaner_GetDungeonRunId(oItemArea)) + ")");
					NWBash_Info("  action: " + GetLocalString(oTarget, "cleaner_action"));
				}
			}
			NWBash_Info("");
		}



		NWBash_Info("Current area: " + _Cleaner_ObjectInfo(oArea));
		if(oMasterArea == oArea){
			int nSlavesCount = Cleaner_GetSlaveAreaCount(oMasterArea);
			if(nSlavesCount > 0){
				NWBash_Info("Is a master area with " + IntToString(nSlavesCount) + " slave areas (multi-area dungeon)");
				NWBash_Info("Slave areas:");
				int i;
				for(i = 0 ; i < nSlavesCount ; i++)
					NWBash_Info("  " + IntToString(i) + ": " + _Cleaner_ObjectInfo(Cleaner_GetSlaveArea(oMasterArea, i)));
			}
			else{
				NWBash_Info("Is a master area with no slaves (single area)");
			}
		}
		else{
			NWBash_Info("Is a slave area of " + _Cleaner_ObjectInfo(oMasterArea));
		}

		int nRunID = Cleaner_GetDungeonRunId(oMasterArea);
		NWBash_Info("Run ID: " + IntToString(nRunID) + " " + (nRunID == 0 ? "(area is inactive / cleaned)" : "(area is active or paused)"));

		int nHasPC = Cleaner_GetIsPcInDungeon(oMasterArea);
		if(nHasPC)
			NWBash_Info("Players in dungeon: 1 or more (blocking dungeon cleaning)");
		else
			NWBash_Info("Players in dungeon: 0");

		int nHasCleanLocks = _Cleaner_Watchdog_GetHasCleanLocks(oMasterArea);
		if(nHasCleanLocks)
			NWBash_Info("Players in transition: 1 or more (blocking dungeon cleaning)");
		else
			NWBash_Info("Players in transition: 0");

		return 0;
	}
	else if(sSubCommand == "clean"){
		int bHelp = NWBash_GetBooleanArg(args, "h|help");
		if(bHelp){
			NWBash_Info("Usage: cleaner clean [options]");
			NWBash_Info("");
			NWBash_Info("options can be:");
			NWBash_Info("-f, --force  Clean the area even if there is players inside");
			return 0;
		}
		int bForce = NWBash_GetBooleanArg(args, "f|force");

		object oTarget = NWBash_GetTarget();
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 1;
		}

		object oAreaToClean = Cleaner_GetMasterArea(oTarget);
		if(!GetIsObjectValid(oAreaToClean)){
			NWBash_Err("Invalid area");
			return 2;
		}

		int nRes = Cleaner_CleanDungeonNow(oAreaToClean, bForce);
		if(nRes){
			NWBash_Info(GetName(oAreaToClean) + " has been reset");
			return 0;
		}
		else{
			NWBash_Err(GetName(oAreaToClean) + " could not be reset. Maybe there is some players inside the area?");
			return 3;
		}
	}
	else if(sSubCommand == "export-master"){
		int bHelp = NWBash_GetBooleanArg(args, "h|help");
		if(bHelp){
			NWBash_Info("Usage: cleaner export-master <var_name>");
			NWBash_Info("Get the master area of the target and saves it inside an environment variable");
			NWBash_Info("");
			NWBash_Info("var_name: name of the environment variable");
			return 0;
		}
		object oTarget = NWBash_GetTarget();
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 1;
		}

		if(ArrayGetLength(args) != 1){
			NWBash_Err("Wrong number of arguments");
			return 2;
		}
		string sVar = ArrayGetString(args, 0);


		object oMasterArea = Cleaner_GetMasterArea(oTarget);
		if(!GetIsObjectValid(oMasterArea)){
			NWBash_Err("Invalid master area");
			return 3;
		}

		NWBash_SetEnvVar(sVar, "0x" + ObjectToString(oMasterArea));
		NWBash_Info("export " + sVar + "=0x" + ObjectToString(oMasterArea) + " (" + GetName(oMasterArea) + ")");
		return 0;
	}
	else if(sSubCommand == "export-slave"){
		int bHelp = NWBash_GetBooleanArg(args, "h|help");
		if(bHelp){
			NWBash_Info("Usage: cleaner export-slave <slave_index> <var_name>");
			NWBash_Info("Get a slave area of the target's master area and saves it inside an environment variable");
			NWBash_Info("");
			NWBash_Info("slave_index: index of the slave area in the slave areas list");
			NWBash_Info("var_name: name of the environment variable");
			return 0;
		}
		object oTarget = NWBash_GetTarget();
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 1;
		}

		if(ArrayGetLength(args) != 2){
			NWBash_Err("Wrong number of arguments");
			return 2;
		}
		int nIndex = ArrayGetInt(args, 0);
		string sVar = ArrayGetString(args, 1);


		object SlaveArea = Cleaner_GetSlaveArea(Cleaner_GetMasterArea(oTarget), nIndex);
		if(!GetIsObjectValid(SlaveArea)){
			NWBash_Err("Invalid slave area");
			return 3;
		}

		NWBash_SetEnvVar(sVar, "0x" + ObjectToString(SlaveArea));
		NWBash_Info("export " + sVar + "=0x" + ObjectToString(SlaveArea) + " (" + GetName(SlaveArea) + ")");
		return 0;
	}
	else{
		NWBash_Err("Invalid subcommand '" + sSubCommand + "'");
		return 126;
	}


	return 127;
}


