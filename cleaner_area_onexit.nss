//
// Cleaner Area OnExit script
//
// Triggers CLEANER_EVENT_CLEAN if there is no one left in the dungeon
//


#include "cleaner_inc"


void CleaningLock(object oMasterArea, object oPC){
	if(GetIsObjectValid(oPC) && !GetIsObjectValid(GetArea(oPC))){
		//Wait oPC has entered the new area
		DelayCommand(1.0, CleaningLock(oMasterArea, oPC));
	}
	else{
		DeleteLocalInt(oMasterArea, "cleaner_lock_"+ObjectToString(oPC));
	}
}

int __GetIsPCInArea(object oArea, object oException=OBJECT_INVALID, int bIgnoreDMs=TRUE){
	int bIsTrigger = GetObjectType(oArea) == OBJECT_TYPE_TRIGGER;

	object oPC=GetFirstPC();
	while(GetIsObjectValid(oPC))
	{
		if((bIsTrigger ? GetIsInSubArea(oPC, oArea) : (GetArea(oPC) == oArea))
			&& oPC!=oException
			&& (bIgnoreDMs? !(GetIsDM(oPC) || GetIsDMPossessed(oPC)) : TRUE) ){
			return TRUE;
		}
		oPC=GetNextPC();
	}
	return FALSE;
}

void main()
{
	object oArea = OBJECT_SELF;
	object oPC = GetExitingObject();

	if(GetIsPC(oPC)){
		SetLocalObject(oPC, "cleaner_areafrom", oArea);

		object oMasterArea = Cleaner_GetMasterArea(oArea);
		SetLocalInt(oMasterArea, "cleaner_lock_"+ObjectToString(oPC), TRUE);
		CleaningLock(oMasterArea, oPC);

		if(!GetIsDM(oPC) && !GetIsDMPossessed(oPC))
			_Cleaner_ExecuteEventScript(oArea, CLEANER_EVENT_EXIT);

		if(!__GetIsPCInArea(oArea, oPC)){
			_Cleaner_PauseArea(oArea);
		}
	}
}