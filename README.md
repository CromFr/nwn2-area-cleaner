
# AreaCleaner

This set of scripts helps resetting areas on a multiplayer NWN2 server to their initial state when all players leave the area, so new players can re-enter the area as if the server has just booted.

It is especially useful for complex dungeons where players interact with the environment (destroy placeables, pick locks, traps, solve puzzles, ...), to ensure players can complete them multiple times without requiring a server reboot.

### Features

- **Area cleaning**:
    + Destroy encounter creatures, loot bags and items on the floor
    + Restore door / chest locks and traps to their original state
    + Reduce server load by deactivating creatures AI
    + Execute specific actions or scripts on specific area objects (like for destroying objects, removing VFX, ...)
    + Gives some time for the player to reconnect before the area is cleaned
- **Area items tracking**: tracks items that are tied to the area in order to remove them (or trigger other actions) when the owner leaves the area (like a key that must be obtained every time the player enters the dungeon). The tracked items can be shared with other players in the dungeon, and players can even disconnect and reconnect with them (unless the dungeon has been cleaned).
- **Multi-area dungeons**: Multiple areas can be tied together to be initialized and cleaned synchronously when there is no players in all areas of the dungeon.
- **Dungeon variables**: local variables that are automatically reset when the dungeon is cleaned.
- **Dialog scripts**: for performing some actions from dialogs, like getting/setting cleaner variables, creating tracked items, etc.


# Installing

### Demo module

This repository is a NWN2 module containing a very small dungeon.

1. [Download](https://gitlab.com/CromFr/nwn2-area-cleaner/-/archive/main/nwn2-area-cleaner-main.zip) or clone this repository into `Documents\Neverwinter Nights 2\Modules\`
2. Open the NWN2 toolset and open the directory module `nwn2-area-cleaner`
3. Compile all scripts (don't worry if `nwbash_s_cleaner` fails to compile)
4. Launch the game, start a new module, and select `nwn2-area-cleaner`

### In your module

1. Copy all files starting with `cleaner_` into your module folder
2. Optionally, if you have already installed [NWBash](https://gitlab.com/CromFr/nwbash), you can also copy `nwbash_s_cleaner.nss`, to provide the `!cleaner` chat command that helps debugging the cleaner.
3. Modify `cleaner_config.nss` to adjust the cleaner configuration if needed.
4. Open your module in the NWN2 toolset, and compile all scripts



# Usage

You can find more specific documentation in [cleaner_inc.nss](cleaner_inc.nss).

## Dungeon lifetime

A dungeon is a group of one or more areas. Most dungeons are one single area, but in some cases you may need to link a set of areas together to perform the dungeon cleaning only when there is no players left in all those areas.

### Active
When a player enters an empty dungeon, it is initialized.

Two types of initialization are run:
1. `MODULE_INIT`, only happens once every server reboot
    - Setups links between the areas of the multi-area dungeon
    - Saves the default day/night/battle musics of each area
    - Saves the state of each door and placeable (opened/closed, locked, trapped) in each area
    - Triggers the cleaner_event_script (local variable set on the area) of each area of the dungeon, with the `CLEANER_EVENT_MODULE_INIT` event ID
2. `INIT`, happens when a player enters the area and it is empty (and has been cleaned)
    - Triggers the cleaner_event_script of each area of the dungeon, with the `CLEANER_EVENT_INIT` event ID

### Paused
If all players in an active area leave the area, the area is paused. While the area is paused, creatures are "ScriptHidden" to reduce server load.

If there is no PC in the dungeon, no PC in transition between this dungeon and other areas, and after a fixed amount of time (see `_CLEANER_AREA_CLEANUP_DELAY_ROUNDS`), the dungeon is cleaned.

### Cleaning
Most tasks are performed when the dungeon is cleaned.

Tasks are:
- Trigger the cleaner_event_script of each area of the dungeon, with the `CLEANER_EVENT_CLEAN` event 
- Destroy encounter creatures, loot bags and items on the floor
- Restore door / chest open/close state, locks and traps to their original state
- Execute actions all area objects and registered objects
- Execute actions on registered items on players

Once the dungeon has been cleaned, it can be activated again when a player enters the dungeon.



## Areas configuration

In order to clean an area upon exit, you need to:
- Set `cleaner_area_onenter` as the area OnEnter script (or call this script from your custom OnEnter script)
- Set `cleaner_area_onexit` as the area OnExit script (or call this script from your custom OnEwit script)
- Optionally set a custom init script with the area cleaner_event_script local variable. Custom events are triggered when the area is initialized or cleaned. See `CLEANER_EVENT_*` constants.

The cleaner OnEnter / OnExit scripts are meant to be set on all areas in your module. If you want to use the cleaner scripts for only a subset of your module areas, you must call `Cleaner_CheckInventory(GetEnteringObject())` in your OnEnter scripts on areas that are accessible from the areas that use the cleaner scripts. This is required in order to track and execute actions on items that have been registered to a dungeon.

### Multi-areas dungeons
Additionally for multi-area dungeons, you should set the following local variables:
- `cleaner_multiarea_slavestags` on the master area. Defines the list of slave area tags. Each element is separated with `|`. ex: `|area_entry|area_underground|`
- `cleaner_multiarea_mastertag` on each slave area. Defines the tag of the master area linked to this slave area.


## Objects local variables
- `cleaner_action` defines what to do with an area object when the area is cleaned. In most cases you'll want to set to `destroy` for removing the object on area cleaning, but there are a lot more actions, like removing VFX, disabling sounds, lights, execute scripts, ... See `Cleaner_DoActionOnObject` in [`cleaner_inc`](cleaner_inc.nss) for the full list of actions.
- `cleaner_flags` allows to prevent specific cleaner behaviour, like preventing lock/trap state restoring and scripthidden creatures. See `Cleaner_*Flags` functions in [`cleaner_inc`](cleaner_inc.nss).

