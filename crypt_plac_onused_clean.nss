#include "cleaner_inc"

void main()
{
	object oAreaToClean = GetObjectByTag("crypt_level_1");
	int nRes = Cleaner_CleanDungeonNow(oAreaToClean);
	if(nRes){
		SpeakString(GetName(oAreaToClean) + " has been reset");
	}
	else{
		SpeakString(GetName(oAreaToClean) + " could not be reset. Maybe there is some players inside the area?");
	}
}
