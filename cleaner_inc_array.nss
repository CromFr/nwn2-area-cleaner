// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// |||||||||||||||||||||||||||||  BIBLIOTHEQUE GERANT DES TABLEAUX  ||||||||||||||||||||||||||||||||||||
// /////////////////////////////////////////////////////////////////////////////////////////////////////
// Crom

// String-based array
struct array
{
	string data;//Format |data1|data2|data3|
};

//NE PAS TOUCHER !
string ArraySeparator = "|";

//A utiliser avec précaution. Modifie le séparateur utilisé pour délimiter les cellules des tableaux
//Actif uniquement pour l'instance d'execution du programme
//Note : pour stocker des vector/location, le séparateur doit être différent de '#'
void ArraySetDefaultSeparator(string sSep)
{
	ArraySeparator = sSep;
}

//Remet bien le séparateur habituel
void ArrayResetDefaultSeparator()
{
	ArraySeparator = "|";
}

//Créé un tableau avec des données déja existantes
// Chaque cellule doit être entourée d'un |
struct array Array(string sArrayData="")
{
	struct array aArray;
	if(sArrayData=="")	aArray.data = ArraySeparator;
	else				aArray.data = sArrayData;
	return aArray;
}

//Retourne les données contenues dans le tableau sous la forme d'un string
// dont chaque cellule est entourée par un |
string ArrayGetData(struct array aArray)
	{return aArray.data;}

//Retourne TRUE si le tablear est vide
int ArrayIsEmpty(struct array aArray)
{
	return FindSubString(aArray.data, ArraySeparator, 1) < 0;
}

//Retourne la taille du tableau
int ArrayGetLength(struct array aArray)
{
	int nCount=0;

	int nBegin = 0;
	int nNext = FindSubString(aArray.data, ArraySeparator, nBegin);
	if(nNext==-1)return 0;
	while(nNext!=-1)
	{
		nCount++;

		nBegin = nNext+1;
		nNext = FindSubString(aArray.data, ArraySeparator, nBegin);
	}

	return nCount-1;
}

//Supprime un index, raccourcissant le tableau
// Attention: n ne doit pas être >= à la taille du tableau
struct array ArrayRemove(struct array aArray, int n){
	int nBeginPos=-1;
	int i;
	for(i=0; i<=n ; i++)
	{
		nBeginPos = FindSubString(aArray.data, ArraySeparator, nBeginPos+1);
	}

	int nEndPos = FindSubString(aArray.data, ArraySeparator, nBeginPos+1);

	aArray.data = GetStringLeft(aArray.data, nBeginPos)
	             +GetStringRight(aArray.data, GetStringLength(aArray.data)-nEndPos);
	return aArray;
}

// Supprime toutes les occurences de sValue dans le tableau
struct array ArrayRemoveValueString(struct array aArray, string sValue){
	sValue = ArraySeparator + sValue + ArraySeparator;
	int nValueLen = GetStringLength(sValue);

	int nOffset = 0;
	int nPos = FindSubString(aArray.data, sValue, nOffset);
	while(nPos >= 0){
		aArray.data = GetStringLeft(aArray.data, nPos) + GetSubString(aArray.data, nPos + nValueLen - 1, -1);
		nOffset = nPos;
		nPos = FindSubString(aArray.data, sValue, nOffset);
	}
	return aArray;
}

// Supprime le premier élément du tableau
struct array ArrayPopFront(struct array aArray){
	int nOffset = FindSubString(aArray.data, ArraySeparator, 1);
	if(nOffset >= 0)
		aArray.data = GetStringRight(aArray.data, GetStringLength(aArray.data) - nOffset);
	return aArray;
}


// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// |||||||||||||||||||||||||||||  APPEND  ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// /////////////////////////////////////////////////////////////////////////////////////////////////////

//Ajoute une donnée de type string à la fin du tableau
struct array ArrayAppendString(struct array aArray, string sData)
{
	aArray.data = aArray.data+sData+ArraySeparator;
	return aArray;
}

//Ajoute une donnée de type int à la fin du tableau
struct array ArrayAppendInt(struct array aArray, int nData)
	{return ArrayAppendString(aArray, IntToString(nData));}

//Ajoute une donnée de type float à la fin du tableau
struct array ArrayAppendFloat(struct array aArray, float fData)
	{return ArrayAppendString(aArray, FloatToString(fData));}

//Ajoute une donnée de type object à la fin du tableau
struct array ArrayAppendObject(struct array aArray, object oData)
	{return ArrayAppendString(aArray, IntToString(ObjectToInt(oData)));}

//Ajoute une donnée de type vector au début du tableau
struct array ArrayAppendVector(struct array aArray, vector vData)
	{return ArrayAppendString(aArray, FloatToString(vData.x)+"#"+FloatToString(vData.y)+"#"+FloatToString(vData.z));}

//Ajoute une donnée de type location à la fin du tableau
struct array ArrayAppendLocation(struct array aArray, location lData)
{
	string sArea = ObjectToString(GetAreaFromLocation(lData));
	vector p = GetPositionFromLocation(lData);
	string sPosition = FloatToString(p.x)+"#"+FloatToString(p.y)+"#"+FloatToString(p.z);
	string sFacing = FloatToString(GetFacingFromLocation(lData));
	return ArrayAppendString(aArray, sArea+"#"+sPosition+"#"+sFacing);
}


// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// |||||||||||||||||||||||||||||  PREPEND  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// /////////////////////////////////////////////////////////////////////////////////////////////////////

//Ajoute une donnée de type string au début du tableau
struct array ArrayPrependString(struct array aArray, string sData)
{
	aArray.data = ArraySeparator+sData+aArray.data;
	return aArray;
}

//Ajoute une donnée de type string au début du tableau
struct array ArrayPrependInt(struct array aArray, int nData)
	{return ArrayPrependString(aArray, IntToString(nData));}

//Ajoute une donnée de type float au début du tableau
struct array ArrayPrependFloat(struct array aArray, float fData)
	{return ArrayPrependString(aArray, FloatToString(fData));}

//Ajoute une donnée de type object au début du tableau
struct array ArrayPrependObject(struct array aArray, object oData)
	{return ArrayPrependString(aArray, ObjectToString(oData));}

//Ajoute une donnée de type vector au début du tableau
struct array ArrayPrependVector(struct array aArray, vector vData)
	{return ArrayPrependString(aArray, FloatToString(vData.x)+"#"+FloatToString(vData.y)+"#"+FloatToString(vData.z));}

//Ajoute une donnée de type location au début du tableau
struct array ArrayPrependLocation(struct array aArray, location lData)
{
	string sArea = ObjectToString(GetAreaFromLocation(lData));
	vector p = GetPositionFromLocation(lData);
	string sPosition = FloatToString(p.x)+"#"+FloatToString(p.y)+"#"+FloatToString(p.z);
	string sFacing = FloatToString(GetFacingFromLocation(lData));
	return ArrayPrependString(aArray, sArea+"#"+sPosition+"#"+sFacing);
}

// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// |||||||||||||||||||||||||||||  GET  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// /////////////////////////////////////////////////////////////////////////////////////////////////////

//Renvoie la valeur string de la n-ième cellule
string ArrayGetString(struct array aArray, int n)
{
	int nCount=0;

	int nBegin = 0;
	int nNext = FindSubString(aArray.data, ArraySeparator, nBegin);

	while(nNext!=-1 && nCount<=n)
	{
		nCount++;

		nBegin = nNext+1;
		nNext = FindSubString(aArray.data, ArraySeparator, nBegin);
	}

	if(nNext == -1)
		return "";
	return GetSubString(aArray.data, nBegin, nNext-nBegin);
}

//Renvoie la valeur int de la n-ième cellule
int ArrayGetInt(struct array aArray, int n)
	{return StringToInt(ArrayGetString(aArray, n));}

//Renvoie la valeur float de la n-ième cellule
float ArrayGetFloat(struct array aArray, int n)
	{return StringToFloat(ArrayGetString(aArray, n));}

//Renvoie la valeur object de la n-ième cellule
object ArrayGetObject(struct array aArray, int n)
	{return IntToObject(StringToInt(ArrayGetString(aArray, n)));}

//Renvoie la valeur vector de la n-ième cellule
// Risque de renvoyer des données vraiment foireuses si le contenu n'est pas un vector ou est vide
// NON TESTE
vector ArrayGetVector(struct array aArray, int n)
{
	string sData = ArrayGetString(aArray, n);

	vector v;

	int nBeginPos = 0;
	int nEndPos = FindSubString(aArray.data, "#", nBeginPos);
	v.x = StringToFloat(GetSubString(aArray.data, nBeginPos+1, nEndPos));

	nBeginPos = nEndPos+1;
	nEndPos = FindSubString(aArray.data, "#", nBeginPos);
	v.y = StringToFloat(GetSubString(aArray.data, nBeginPos+1, nEndPos));

	nBeginPos = nEndPos+1;
	v.y = StringToFloat(GetSubString(aArray.data, nBeginPos+1, GetStringLength(aArray.data)));

	return v;
}

//Renvoie la valeur location de la n-ième cellule
// Risque de renvoyer des données vraiment foireuses si le contenu n'est pas une location ou est vide
// NON TESTE
location ArrayGetLocation(struct array aArray, int n)
{
	string sData = ArrayGetString(aArray, n);

	object oArea;
	vector vPos;
	float fFacing;

	int nBeginPos = 0;
	int nEndPos = FindSubString(aArray.data, "#", nBeginPos);
	oArea = IntToObject(StringToInt(GetSubString(aArray.data, nBeginPos+1, nEndPos)));

	nBeginPos = nEndPos+1;
	nEndPos = FindSubString(aArray.data, "#", nBeginPos);
	vPos.x = StringToFloat(GetSubString(aArray.data, nBeginPos+1, nEndPos));

	nBeginPos = nEndPos+1;
	nEndPos = FindSubString(aArray.data, "#", nBeginPos);
	vPos.y = StringToFloat(GetSubString(aArray.data, nBeginPos+1, nEndPos));

	nBeginPos = nEndPos+1;
	nEndPos = FindSubString(aArray.data, "#", nBeginPos);
	vPos.z = StringToFloat(GetSubString(aArray.data, nBeginPos+1, nEndPos));

	nBeginPos = nEndPos+1;
	fFacing = StringToFloat(GetSubString(aArray.data, nBeginPos+1, GetStringLength(aArray.data)));

	return Location(oArea, vPos, fFacing);
}

// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// |||||||||||||||||||||||||||||  SET  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// /////////////////////////////////////////////////////////////////////////////////////////////////////

//Modifie la valeur dans la n-ième cellule par le contenu string donné
// Si la cellule est située en dehors du tableau, des cellules vides seront créées pour combler le vide
struct array ArraySetString(struct array aArray, int n, string sData)
{
	int i;
	int nSize = ArrayGetLength(aArray);
	if(n>=nSize)
	{
		for(i=0 ; i<n-nSize+1 ; i++)
		{
			ArrayAppendString(aArray, "");
		}
		ArrayAppendString(aArray, sData);
		return aArray;
	}

	int nBeginPos=-1;
	for(i=0; i<=n ; i++)
	{
		nBeginPos = FindSubString(aArray.data, ArraySeparator, nBeginPos+1);
	}

	nBeginPos++;

	int nEndPos = FindSubString(aArray.data, ArraySeparator, nBeginPos);

	aArray.data = GetStringLeft(aArray.data, nBeginPos)
	             +sData
	             +GetStringRight(aArray.data, GetStringLength(aArray.data)-nEndPos);
	return aArray;
}

//Modifie la valeur dans la n-ième cellule par le contenu int donné
struct array ArraySetInt(struct array aArray, int n, int nData)
	{return ArraySetString(aArray, n, IntToString(nData));}

//Modifie la valeur dans la n-ième cellule par le contenu float donné
struct array ArraySetFloat(struct array aArray, int n, float fData)
	{return ArraySetString(aArray, n, FloatToString(fData));}

//Modifie la valeur dans la n-ième cellule par le contenu object donné
struct array ArraySetObject(struct array aArray, int n, object oData)
	{return ArraySetString(aArray, n, ObjectToString(oData));}

//Modifie la valeur dans la n-ième cellule par le contenu vector donné
struct array ArraySetVector(struct array aArray, int n, vector vData)
	{return ArraySetString(aArray, n, FloatToString(vData.x)+"#"+FloatToString(vData.y)+"#"+FloatToString(vData.z));}

//Modifie la valeur dans la n-ième cellule par le contenu location donné
struct array ArraySetLocation(struct array aArray, int n, location lData)
{
	string sArea = ObjectToString(GetAreaFromLocation(lData));
	vector p = GetPositionFromLocation(lData);
	string sPosition = FloatToString(p.x)+"#"+FloatToString(p.y)+"#"+FloatToString(p.z);
	string sFacing = FloatToString(GetFacingFromLocation(lData));
	return ArraySetString(aArray, n, sArea+"#"+sPosition+"#"+sFacing);
}

// Renvoit TRUE si la valeur est présente dans le tableau
int ArrayGetHasValueString(struct array aArray, string s){
	return FindSubString(aArray.data, ArraySeparator + s + ArraySeparator) >= 0;
}
// Renvoit TRUE si la valeur est présente dans le tableau
int ArrayGetHasValueInt(struct array aArray, int n){
	return ArrayGetHasValueString(aArray, IntToString(n));
}
// Renvoit TRUE si la valeur est présente dans le tableau
int ArrayGetHasValueFloat(struct array aArray, float f){
	return ArrayGetHasValueString(aArray, FloatToString(f));
}
// Renvoit TRUE si la valeur est présente dans le tableau
int ArrayGetHasValueObject(struct array aArray, object o){
	return ArrayGetHasValueString(aArray, ObjectToString(o));
}
// Renvoit TRUE si la valeur est présente dans le tableau
int ArrayGetHasValueVector(struct array aArray, vector v){
	return ArrayGetHasValueString(aArray, FloatToString(v.x)+"#"+FloatToString(v.y)+"#"+FloatToString(v.z));
}
// Renvoit TRUE si la valeur est présente dans le tableau
int ArrayGetHasValueLocation(struct array aArray, location l){
	string sArea = ObjectToString(GetAreaFromLocation(l));
	vector p = GetPositionFromLocation(l);
	string sPosition = FloatToString(p.x)+"#"+FloatToString(p.y)+"#"+FloatToString(p.z);
	string sFacing = FloatToString(GetFacingFromLocation(l));
	return ArrayGetHasValueString(aArray, sArea+"#"+sPosition+"#"+sFacing);
}

// Mélange un tableau (attention, mauvaises performances)
struct array ArrayShuffle(struct array aArray){
	int nLen = ArrayGetLength(aArray);

	int i;
	for(i = nLen - 1 ; i > 0 ; i--)
	{
		int j = Random(i);

		string sValue = ArrayGetString(aArray, i);
		aArray = ArraySetString(aArray, i, ArrayGetString(aArray, j));
		aArray = ArraySetString(aArray, j, sValue);
	}
	return aArray;
}