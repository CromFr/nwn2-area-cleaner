// ==================================================
// CLEANER CONFIG
// ==================================================

// If TRUE, messages will be broadcasted to track area states
const int _CLEANER_DEBUG = TRUE;

// Delay in rounds before an empty area is cleaned, allowing the player to come back
const int _CLEANER_AREA_CLEANUP_DELAY_ROUNDS = 20;


// Reimplement this function to log error messages
void _Cleaner_SignalBug(string sMsg){
	if(_CLEANER_DEBUG){
		object oPC = GetFirstPC();
		while(GetIsObjectValid(oPC))
		{
			SendMessageToPC(oPC, "[CLEANER BUG]: " + sMsg);
			oPC = GetNextPC();
		}
	}
	else
		SendMessageToAllDMs("[CLEANER BUG]: " + sMsg);

	WriteTimestampedLogEntry("[CLEANER BUG]: " + sMsg);
}

// Reimplement this function to add custom cleaner actions to execute on object that are being cleaned
// This function is used inside Cleaner_DoActionOnObject.
//
// This function must return TRUE if the action exists, FALSE if the action is not recognized.
int _Cleaner_DoExtraActionOnObject(object oObject, string sActName, string sActValue){
	// // Example implementation:
	// if(sActName == "custom")
	// 	SetTag(oObject, "Something");
	// else if(sActName == "set-str-value")
	// 	SetLocalString(oObject, "str-value", sActValue);
	// else
	// 	return FALSE;
	// return TRUE;

	return FALSE;
}
