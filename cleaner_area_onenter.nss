//
// Cleaner Area OnEnter script
//
// Triggers CLEANER_EVENT_MODULE_INIT once per reboot
// Triggers CLEANER_EVENT_INIT if nobody is in the dungeon
// Executes actions on registered items in players inventory
//


#include "cleaner_inc"
#include "cleaner_inc_array"

int __GetIsPCInArea(object oArea, object oException=OBJECT_INVALID, int bIgnoreDMs=TRUE){
	int bIsTrigger = GetObjectType(oArea) == OBJECT_TYPE_TRIGGER;

	object oPC=GetFirstPC();
	while(GetIsObjectValid(oPC))
	{
		if((bIsTrigger ? GetIsInSubArea(oPC, oArea) : (GetArea(oPC) == oArea))
			&& oPC!=oException
			&& (bIgnoreDMs? !(GetIsDM(oPC) || GetIsDMPossessed(oPC)) : TRUE) ){
			return TRUE;
		}
		oPC=GetNextPC();
	}
	return FALSE;
}

void InitArea(object oArea, int nEvent){
	_Cleaner_ExecuteEventScript(oArea, nEvent);
	_Cleaner_SaveAreaState(oArea);
	SetLocalInt(oArea, "cleaner_module_init", TRUE);
}

void main()
{
	object oArea = OBJECT_SELF;
	object oPC = GetEnteringObject();

	if(GetIsPC(oPC)){
		if(GetIsDM(oPC) && GetLocalInt(oPC, "dm_is_invisible"))
			return;
		int bActivated = FALSE;

		//Module init dungeon
		if(GetLocalInt(oArea, "cleaner_module_init") == FALSE){
			//Init multi area dungeons
			object oMasterArea;
			string sMasterTag = GetLocalString(oArea, "cleaner_multiarea_mastertag");
			if(sMasterTag != "")
				oMasterArea = GetObjectByTag(sMasterTag);
			else{
				oMasterArea = Cleaner_GetMasterArea(oArea);
				SetLocalString(oArea, "cleaner_multiarea_mastertag", GetTag(oMasterArea));
			}

			if(_CLEANER_DEBUG)
				_Cleaner_BroadcastMsg("<b><c=orange>============ MODULE_INIT DUNGEON "+GetTag(oMasterArea)+" ============</c></b>");

			string sSlaveAreasTags = GetLocalString(oMasterArea, "cleaner_multiarea_slavestags");
			if(sSlaveAreasTags != ""){
				struct array aSlaveAreasTags = Array(sSlaveAreasTags);
				int nAreasCount = ArrayGetLength(aSlaveAreasTags);
				int i;
				for(i = 0 ; i < nAreasCount ; i++){
					string sSlaveTag = ArrayGetString(aSlaveAreasTags, i);

					int iSlave = 0;
					object oSlaveArea = GetObjectByTag(sSlaveTag, iSlave);
					while(GetIsObjectValid(oSlaveArea))
					{
						if(!GetIsObjectValid(oSlaveArea)){
							_Cleaner_SignalBug("Could not get area by tag='"+sSlaveTag+"' in "+_Cleaner_ObjectInfo(oMasterArea)+" cleaner_multiarea_slavestags");
						}
						else{
							if(GetLocalString(oSlaveArea, "cleaner_multiarea_mastertag") != GetTag(oMasterArea)){
								_Cleaner_SignalBug("warning: Area "+_Cleaner_ObjectInfo(oSlaveArea)+" is a slave of "+_Cleaner_ObjectInfo(oMasterArea)+" but cleaner_multiarea_mastertag is not correct\n"
									+"Please set local var cleaner_multiarea_mastertag = '"+GetTag(oMasterArea)+"' on area "+_Cleaner_ObjectInfo(oSlaveArea));
							}
							Cleaner_AddSlaveArea(oMasterArea, oSlaveArea);
						}

						oSlaveArea = GetObjectByTag(sSlaveTag, ++iSlave);
					}

				}
			}

			//Signal module init
			InitArea(oMasterArea, CLEANER_EVENT_MODULE_INIT);

			int nSlaveCount = Cleaner_GetSlaveAreaCount(oMasterArea);
			int i;
			for(i = 0 ; i < nSlaveCount ; i++){
				object oSlaveArea = Cleaner_GetSlaveArea(oMasterArea, i);
				InitArea(oSlaveArea, CLEANER_EVENT_MODULE_INIT);
			}
		}

		//Init dungeon
		object oMasterArea = Cleaner_GetMasterArea(oArea);
		object oAreaFrom = GetLocalObject(oPC, "cleaner_areafrom");
		if(!GetIsObjectValid(oAreaFrom) || !Cleaner_GetIsAreaInDungeon(oAreaFrom, oMasterArea)){
			//oPC enters the dungeon from outside
			if(GetLocalInt(oMasterArea, "cleaner_runid") == 0){
				//Nobody in the dungeon
				if(_CLEANER_DEBUG)
					_Cleaner_BroadcastMsg("<b><c=orange>============ INIT DUNGEON "+GetTag(oMasterArea)+" ============</c></b>");
				bActivated = TRUE;

				int nRunID = Random(2147483646) + 1;
				SetLocalInt(oMasterArea, "cleaner_runid", nRunID);
				_Cleaner_InitSingleArea(oMasterArea);

				int nSlaveCount = Cleaner_GetSlaveAreaCount(oMasterArea);
				int i;
				for(i = 0 ; i < nSlaveCount ; i++){
					object oSlaveArea = Cleaner_GetSlaveArea(oMasterArea, i);
					_Cleaner_InitSingleArea(oSlaveArea);
				}

				// Trigger cleaner watchdog
				AssignCommand(oMasterArea, _Cleaner_Watchdog(nRunID));
			}
		}

		if(!bActivated && !__GetIsPCInArea(oArea, oPC)){
			_Cleaner_ResumeArea(oArea);
		}

		//Clean PC items
		Cleaner_CheckInventory(oPC);

		// Trigger event script
		if(!GetIsDM(oPC) && !GetIsDMPossessed(oPC))
			_Cleaner_ExecuteEventScript(oArea, CLEANER_EVENT_ENTER);
	}
}


