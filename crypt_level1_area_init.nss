// Cleaner event script

#include "cleaner_inc"

void RecurrentDamage(object oArea, int nRunID);

void main(int nEvent)
{
	object oArea = OBJECT_SELF;

	switch(nEvent)
	{
		case CLEANER_EVENT_MODULE_INIT:{
			// Executed only once
			// Mainly used to setup things that must persist across cleanings.
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_MODULE_INIT");
		}break;

		case CLEANER_EVENT_INIT:{
			// Executed when a player enters an inactive area
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_INIT");

			// Chose a random secret number
			Cleaner_SetInt("secret_number", Random(4) + 1);

			// Create a chest at a random location
			object oChest = CreateObject(
				OBJECT_TYPE_PLACEABLE,
				"plc_ml_chestm01",
				GetLocation(GetObjectByTag("crypt_wp_random_chest", Random(3)))
			);

			// Destroy the chest on area exit, so we never have two chests
			Cleaner_SetObjectOnCleanAction(oChest, "destroy");

			// Create the dungeon key in the chest
			object oKey = CreateItemOnObject("crypt_key", oChest);

			// Destroy it if it leaves the dungeon
			Cleaner_RegisterItemOnCleanAction(oKey, oArea, "destroy");

			// Start RecurrentDamage to will inflict damage every round to all
			// creatures in the area, and stops when the area is cleaned
			RecurrentDamage(oArea, Cleaner_GetDungeonRunId(oArea));
		}break;

		case CLEANER_EVENT_CLEAN:
			// Executed just before the area is cleaned
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_CLEAN");
			break;

		case CLEANER_EVENT_ENTER:
			// Executed every time a player enters the area
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_ENTER");

			DelayCommand(3.0, FloatingTextStringOnCreature("This area is filled with acid ! hurry up !", GetEnteringObject(), FALSE));
			break;

		case CLEANER_EVENT_EXIT:
			// Executed every time a player exits the area
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_EXIT");
			break;
	}
}

void RecurrentDamage(object oArea, int nRunID){
	// Exit if the area has been cleaned
	if(Cleaner_GetDungeonRunId(oArea) != nRunID)
		return;

	// Damage all creatures in the area
	object oCreature = GetFirstObjectInArea(oArea);
	while(GetIsObjectValid(oCreature)){
		if(GetObjectType(oCreature) == OBJECT_TYPE_CREATURE)
			ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(1, DAMAGE_TYPE_ACID), oCreature);
		oCreature = GetNextObjectInArea(oArea);
	}

	// Continue applying damage
	DelayCommand(6.0, RecurrentDamage(oArea, nRunID));
}
