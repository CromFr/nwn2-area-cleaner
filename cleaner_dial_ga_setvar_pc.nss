// Sets a cleaner variable that is tied to the player.
// Internally, this will set a cleaner variable named <sVarName><pc_object_id>.
//
// sType: int, float, string
// sName: variable name
// sValue: variable value. The value will be converted to the appropriate type

#include "cleaner_inc"

void main(string sType, string sPrefix, string sValue)
{
	object oNPC = OBJECT_SELF;
	object oPC = GetPCSpeaker();

	int nType;
	if(sType == "int")         Cleaner_SetInt(sPrefix + ObjectToString(oPC), StringToInt(sValue));
	else if(sType == "float")  Cleaner_SetFloat(sPrefix + ObjectToString(oPC), StringToFloat(sValue));
	else if(sType == "string") Cleaner_SetString(sPrefix + ObjectToString(oPC), sValue);
	else _Cleaner_SignalBug("cleaner_dial_ga_setvar_pc: Invalid variable type '"+sType+"' (in dialog with "+_Cleaner_ObjectInfo(oNPC)+")");
}