// Cleaner event script

#include "cleaner_inc"

void main(int nEvent)
{
	object oArea = OBJECT_SELF;

	switch(nEvent)
	{
		case CLEANER_EVENT_MODULE_INIT:{
			// Executed only once
			// Mainly used to setup things that must persist across cleanings.
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_MODULE_INIT");

			// Register the boulders to be refreshed (re-created) on area cleaning.
			// The registration persists across cleanings, so we only need to do this once.
			object oRocks = GetObjectByTag("crypt_destroyable_rocks");
			Cleaner_RegisterObjectToRefresh(oRocks);
		}break;

		case CLEANER_EVENT_INIT:{
			// Executed when a player enters an inactive area
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_INIT");

			// Spawn final boss
			object oBoss = CreateObject(OBJECT_TYPE_CREATURE, "crypt_gardakan", GetLocation(GetObjectByTag("wp_crypt_gardakan")));

			// Destroy the boss on cleaning
			Cleaner_SetObjectOnCleanAction(oBoss, "destroy");

			// Chose a random secret number to guess
			Cleaner_SetInt("secret_number", Random(4) + 1);
		}break;

		case CLEANER_EVENT_CLEAN:
			// Executed just before the area is cleaned
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_CLEAN");
			break;

		case CLEANER_EVENT_ENTER:
			// Executed every time a player enters the area
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_ENTER");
			break;

		case CLEANER_EVENT_EXIT:
			// Executed every time a player exits the area
			_Cleaner_BroadcastMsg(GetName(oArea) + ": CLEANER_EVENT_EXIT");
			break;
	}
}
